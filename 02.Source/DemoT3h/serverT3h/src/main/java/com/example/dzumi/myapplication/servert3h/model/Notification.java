package com.example.dzumi.myapplication.servert3h.model;

/**
 * Created by dzumi on 26/01/2015.
 */
public class Notification {
    public final static String ID = "Id";
    public final static String TITLE = "title";
    public final static String DESCRIPTION = "description";
    public final static String DATE = "date";
    public final static String SENDER = "sender";
    public final static String IMAGE_URL = "url_image";

    long id;
    String title;
    String description;
    long date;


    String sender;
    String url_image;

    public boolean isRead() {
        return isRead;
    }

    public void setIsRead(boolean isRead) {
        this.isRead = isRead;
    }

    boolean isRead;

    public Notification(long id, String title, String description, long date, String sender, String url_image, boolean isRead) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.date = date;
        this.sender = sender;
        this.url_image = url_image;
        this.isRead = isRead;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getUrl_image() {
        return url_image;
    }

    public void setUrl_image(String url_image) {
        this.url_image = url_image;
    }

}
