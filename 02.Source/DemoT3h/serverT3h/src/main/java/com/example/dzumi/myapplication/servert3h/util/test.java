package com.example.dzumi.myapplication.servert3h.util;

import com.example.dzumi.myapplication.servert3h.model.Email;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dzumi on 26/01/2015.
 */
public class test {
    public static void main(String [ ] args)
    {
        List<JSONObject> jsonObjects = new ArrayList<>();
//        jsonObjects.add(new JSONObject("APIKey", Constants.API_KEY, JSONObject.TYPE_STRING));

//        jsonObjects.add(new JSONObject("status", "1" , JSONObject.TYPE_INT));
//        jsonObjects.add(new JSONObject("error_message", "null" , JSONObject.TYPE_INT));
//        jsonObjects.add(new JSONObject("access_token", Constants.ACCESS_TOKEN , JSONObject.TYPE_STRING));

//        jsonObjects.add(new JSONObject("status", "0" , JSONObject.TYPE_INT));
//        jsonObjects.add(new JSONObject("error_message", "Tai khoan hoac mat khau khong hop le" , JSONObject.TYPE_STRING));
//        jsonObjects.add(new JSONObject("access_token", "null" , JSONObject.TYPE_INT));

        jsonObjects.add(new JSONObject("status", "1" , JSONObject.TYPE_INT));
        jsonObjects.add(new JSONObject("error_message", "null" , JSONObject.TYPE_INT));
        StringBuilder builder = new StringBuilder();

        List<Email> emails = new ArrayList<>();
        emails.add(new Email(1l, "Thong bao", "Thong bao chuyen khoan", System.currentTimeMillis()));
        emails.add(new Email(2l, "Hoc phi HK2", "Hoc phi HK2 la 80k$", System.currentTimeMillis()));
        emails.add(new Email(3l, "Ket qua thi lai", "Chuc may man lan sau", System.currentTimeMillis()));

        for(Email email : emails)
        {
            List<JSONObject> temp = new ArrayList<>();
            temp.add(new JSONObject(Email.TITLE, email.getTitle(), JSONObject.TYPE_STRING));
            temp.add(new JSONObject(Email.DESCRIPTION, email.getDescription(), JSONObject.TYPE_STRING));
            temp.add(new JSONObject(Email.ID, email.getId() + "", JSONObject.TYPE_INT));
            temp.add(new JSONObject(Email.DATE, email.getDate() + "", JSONObject.TYPE_INT));
            builder.append(JSONUtil.getJson(temp)).append(",");

        }
        jsonObjects.add(new JSONObject("emails", "[" + builder.substring(0,builder.length() -1) + "]" , JSONObject.TYPE_INT));
        System.out.print(JSONUtil.getJson(jsonObjects));
    }
}
