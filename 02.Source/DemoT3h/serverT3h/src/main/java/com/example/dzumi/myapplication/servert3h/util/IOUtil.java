package com.example.dzumi.myapplication.servert3h.util;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by dzumi on 04/06/2015.
 */
public class IOUtil {


    public boolean deleteDirectory(File directory) {
        if(directory.exists()){
            File[] files = directory.listFiles();
            if(null!=files){
                for(int i=0; i<files.length; i++) {
                    if(files[i].isDirectory()) {
                        deleteDirectory(files[i]);
                    }
                    else {
                        files[i].delete();
                    }
                }
            }
        }
        return(directory.delete());
    }


    public static boolean deleteFolder(String path) {
        File file = new File(path);
        if (file.exists())
            return file.delete();
        return false;
    }

    public static boolean delete(String path) {
        File file = new File(path);
        if (file.exists())
            return file.delete();
        return false;
    }

    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i=0; i<children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        // The directory is now empty so delete it
        return dir.delete();
    }


    public static String readTextFile(String pathFile)
    {
        StringBuilder result = new StringBuilder();
        try {
            FileReader fileReader = new FileReader(pathFile);
            BufferedReader br = new BufferedReader(fileReader);

            int i;
            char[] buffer = new char[512];
            while((i = br.read(buffer))!= -1)
            {
                result.append(new String(buffer, 0, i));
            }
            br.close();
            fileReader.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        }
        return result.toString();
    }
    public static boolean appendStringFile(String pathFile, String text)
    {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(pathFile, true);
            BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);
            text = text+ "\n";
            bos.write(text.getBytes());
            bos.flush();
            bos.close();

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
        return true;

    }

    public static boolean writeStringFile(String pathFile, String text)
    {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(pathFile, true);
            BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);
            text = text+ "\n";
            bos.write(text.getBytes());
            bos.close();

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
        return true;

    }


}
