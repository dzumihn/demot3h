package com.example.dzumi.myapplication.servert3h.util;

import java.util.List;

/**
 * Created by dzumi on 26/01/2015.
 */
public class JSONUtil {
    public static String getJson(List<JSONObject> map)
    {
        StringBuilder builder = new StringBuilder();
        builder.append("{");
        for(JSONObject item : map)
        {
            builder.append("\"").append(item.getKey()).append("\":");
            switch (item.getType())
            {
                case JSONObject.TYPE_STRING:
                    builder.append("\"").append(item.getValues()).append("\",");
                    break;
                default:
                    builder.append(item.getValues()).append(",");
                    break;
            }
        }
        builder.replace(builder.length() - 1, builder.length(), "");
        builder.append("}");
        return builder.toString();
    }
}
