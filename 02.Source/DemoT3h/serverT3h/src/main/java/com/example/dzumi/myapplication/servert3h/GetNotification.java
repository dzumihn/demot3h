package com.example.dzumi.myapplication.servert3h;

import com.example.dzumi.myapplication.servert3h.lib.JSONException;
import com.example.dzumi.myapplication.servert3h.lib.JSONObject;
import com.example.dzumi.myapplication.servert3h.model.Notification;
import com.example.dzumi.myapplication.servert3h.util.Constants;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by dzumi on 26/01/2015.
 */
public class GetNotification extends HttpServlet {
    List<Notification> notifications;
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {


        StringBuffer jb = new StringBuffer();
        String line = null;
        try {
            BufferedReader reader = req.getReader();
            while ((line = reader.readLine()) != null)
                jb.append(line);
        } catch (Exception e) { //report an error
        }
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(jb.toString());
            String access_token =jsonObject.getString("access_token");

        } catch (JSONException e) {
            e.printStackTrace();
        }

//        String access_token = req.getParameter("access_token");


        resp.setContentType(Constants.CONTENT_TYPE_APPLICATION_JSON);

      /*  List<JSONObject> jsonObjects = new ArrayList<>();

        if(access_token.equals(Constants.ACCESS_TOKEN)) {
            initData();
            jsonObjects.add(new JSONObject("status", "1" , JSONObject.TYPE_INT));
            jsonObjects.add(new JSONObject("error_message", "null" , JSONObject.TYPE_INT));
            StringBuilder builder = new StringBuilder();
            for(Email email : emails)
            {
                List<JSONObject> temp = new ArrayList<>();
                temp.add(new JSONObject(Email.TITLE, email.getTitle(), JSONObject.TYPE_STRING));
                temp.add(new JSONObject(Email.DESCRIPTION, email.getDescription(), JSONObject.TYPE_STRING));
                temp.add(new JSONObject(Email.ID, email.getId() + "", JSONObject.TYPE_INT));
                temp.add(new JSONObject(Email.DATE, email.getDate() + "", JSONObject.TYPE_INT));
                builder.append(JSONUtil.getJson(temp)).append(",");

            }
            jsonObjects.add(new JSONObject("emails", "[" + builder.substring(0,builder.length() -1) + "]" , JSONObject.TYPE_INT));
        }
        else
        {
            jsonObjects.add(new JSONObject("status", "0", JSONObject.TYPE_INT));
            jsonObjects.add(new JSONObject("error_message", "Access Token khong dung" , JSONObject.TYPE_STRING));
            jsonObjects.add(new JSONObject("emails", "null" , JSONObject.TYPE_INT));
        }

        resp.getWriter().print(JSONUtil.getJson(jsonObjects));*/

    }

    void initData()
    {
        notifications = new ArrayList<>();
        notifications.add(new Notification(1l, "Th&ocirc;ng b&aacute;o l&#432;&#417;ng", "Th&ocirc;ng b&aacute;o l&#432;&#417;ng th&aacute;ng 8",
                System.currentTimeMillis(), "Ph&ograve;ng nh&acirc;n s&#7921;", "", true));
        notifications.add(new Notification(2l, "Plan th&aacute;ng 8", "C&aacute;c c&ocirc;ng vi&#7879;c c&#7847;n l&agrave;m trong th&aacute;ng 8/2015",
                System.currentTimeMillis(), "Nguy&#7877;n V&#259;n A", "", false));
        notifications.add(new Notification(3l, "Report ng&agrave;y 23/8", "B&aacute;o c&aacute;o c&ocirc;ng vi&#7879;c ng&agrave;y 23/8",
                System.currentTimeMillis(), "Nguy&#7877;n V&#259;n B","", false));
        notifications.add(new Notification(3l, "Report ng&agrave;y 22/8", "B&aacute;o c&aacute;o c&ocirc;ng vi&#7879;c ng&agrave;y 22/8",
                System.currentTimeMillis(), "Nguy&#7877;n V&#259;n B","", false));
        notifications.add(new Notification(3l, "Report ng&agrave;y 20/8", "B&aacute;o c&aacute;o c&ocirc;ng vi&#7879;c ng&agrave;y 20/8",
                System.currentTimeMillis(), "Nguy&#7877;n V&#259;n B","", true));
        notifications.add(new Notification(3l, "Report ng&agrave;y 21/8", "B&aacute;o c&aacute;o c&ocirc;ng vi&#7879;c ng&agrave;y 21/8",
                System.currentTimeMillis(), "Nguy&#7877;n V&#259;n B","", false));
        notifications.add(new Notification(3l, "Report ng&agrave;y 24/8", "B&aacute;o c&aacute;o c&ocirc;ng vi&#7879;c ng&agrave;y 24/8",
                System.currentTimeMillis(), "Nguy&#7877;n V&#259;n B","", true));
        notifications.add(new Notification(3l, "Report ng&agrave;y 25/8", "B&aacute;o c&aacute;o c&ocirc;ng vi&#7879;c ng&agrave;y 25/8",
                System.currentTimeMillis(), "Nguy&#7877;n V&#259;n B","", false));
    }
}
