package com.example.dzumi.myapplication.servert3h;

import com.example.dzumi.myapplication.servert3h.util.Constants;
import com.example.dzumi.myapplication.servert3h.util.JSONObject;
import com.example.dzumi.myapplication.servert3h.util.JSONUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by dzumi on 26/01/2015.
 */
public class GetAPIKey extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
        resp.setContentType(Constants.CONTENT_TYPE_APPLICATION_JSON);
        List<JSONObject> jsonObjects = new ArrayList<>();
        jsonObjects.add(new JSONObject("APIKey", Constants.API_KEY, JSONObject.TYPE_STRING));
        resp.getWriter().println(JSONUtil.getJson(jsonObjects));

    }
}
