/*
   For step-by-step instructions on connecting your Android application to this backend module,
   see "App Engine Java Servlet Module" template documentation at
   https://github.com/GoogleCloudPlatform/gradle-appengine-templates/tree/master/HelloWorld
*/

package com.example.dzumi.myapplication.servert3h;

import com.example.dzumi.myapplication.servert3h.util.Constants;
import com.example.dzumi.myapplication.servert3h.util.IOUtil;
import com.example.dzumi.myapplication.servert3h.util.JSONObject;
import com.example.dzumi.myapplication.servert3h.util.JSONUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import com.example.dzumi.myapplication.servert3h.util.JSONUtil;

//import com.example.dzumi.myapplication.servert3h.util.JSONObject;

public class RegisterUser extends HttpServlet {


    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {

        String userName = req.getParameter("user_name");
        String pass = req.getParameter("pass");
        /*String userName = jsonObject.getString("user_name");
        String pass = jsonObject.getString("pass");
        String APIKey = jsonObject.getString("APIKey");*/

        resp.setContentType(Constants.CONTENT_TYPE_APPLICATION_JSON);

        List<JSONObject> jsonObjects = new ArrayList<>();
        if (userName == null || pass == null) {
            jsonObjects.add(new JSONObject("status", "0", JSONObject.TYPE_INT));
            jsonObjects.add(new JSONObject("error_message", "Loi cu phap", JSONObject.TYPE_STRING));
            jsonObjects.add(new JSONObject("access_token", "null", JSONObject.TYPE_INT));
        } else {
            String text = userName + "$" + pass + "#";
            IOUtil.appendStringFile("./login.txt",text);
            jsonObjects.add(new JSONObject("status", "1", JSONObject.TYPE_INT));
        }
        resp.getWriter().print(JSONUtil.getJson(jsonObjects));

    }
}
