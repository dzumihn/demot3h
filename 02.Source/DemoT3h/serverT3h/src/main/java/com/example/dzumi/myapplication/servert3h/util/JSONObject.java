package com.example.dzumi.myapplication.servert3h.util;

/**
 * Created by dzumi on 26/01/2015.
 */
public class JSONObject {
    public final static int TYPE_INT = 1;
    public final static int TYPE_LONG = 2;
    public final static int TYPE_STRING = 0;
    String key;
    String values;
    int type;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValues() {
        return values;
    }

    public void setValues(String values) {
        this.values = values;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public JSONObject(String key, String values, int type) {

        this.key = key;
        this.values = values;
        this.type = type;
    }
}
