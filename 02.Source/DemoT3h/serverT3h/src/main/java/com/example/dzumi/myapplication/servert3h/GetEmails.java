package com.example.dzumi.myapplication.servert3h;

import com.example.dzumi.myapplication.servert3h.model.Email;
import com.example.dzumi.myapplication.servert3h.util.Constants;
import com.example.dzumi.myapplication.servert3h.util.JSONObject;
import com.example.dzumi.myapplication.servert3h.util.JSONUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by dzumi on 26/01/2015.
 */
public class GetEmails extends HttpServlet {
    List<Email> emails;
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {


       /* StringBuffer jb = new StringBuffer();
        String line = null;
        try {
            BufferedReader reader = req.getReader();
            while ((line = reader.readLine()) != null)
                jb.append(line);
        } catch (Exception e) { *//*report an error*//* }

        org.json.JSONObject jsonObject = new org.json.JSONObject(jb.toString());*/

        String access_token = req.getParameter("access_token");
//        String access_token =jsonObject.getString("access_token");

        resp.setContentType(Constants.CONTENT_TYPE_APPLICATION_JSON);

        List<JSONObject> jsonObjects = new ArrayList<>();

        if(access_token.equals(Constants.ACCESS_TOKEN)) {
            initData();
            jsonObjects.add(new JSONObject("status", "1" , JSONObject.TYPE_INT));
            jsonObjects.add(new JSONObject("error_message", "null" , JSONObject.TYPE_INT));
            StringBuilder builder = new StringBuilder();
            for(Email email : emails)
            {
                List<JSONObject> temp = new ArrayList<>();
                temp.add(new JSONObject(Email.TITLE, email.getTitle(), JSONObject.TYPE_STRING));
                temp.add(new JSONObject(Email.DESCRIPTION, email.getDescription(), JSONObject.TYPE_STRING));
                temp.add(new JSONObject(Email.ID, email.getId() + "", JSONObject.TYPE_INT));
                temp.add(new JSONObject(Email.DATE, email.getDate() + "", JSONObject.TYPE_INT));
                builder.append(JSONUtil.getJson(temp)).append(",");

            }
            jsonObjects.add(new JSONObject("emails", "[" + builder.substring(0,builder.length() -1) + "]" , JSONObject.TYPE_INT));
        }
        else
        {
            jsonObjects.add(new JSONObject("status", "0", JSONObject.TYPE_INT));
            jsonObjects.add(new JSONObject("error_message", "Access Token khong dung" , JSONObject.TYPE_STRING));
            jsonObjects.add(new JSONObject("emails", "null" , JSONObject.TYPE_INT));
        }

        resp.getWriter().print(JSONUtil.getJson(jsonObjects));

    }

    void initData()
    {
        emails = new ArrayList<>();
        emails.add(new Email(1l, "Thong bao", "Thong bao chuyen khoan", System.currentTimeMillis()));
        emails.add(new Email(2l, "Hoc phi HK2", "Hoc phi HK2 la 80k$", System.currentTimeMillis()));
        emails.add(new Email(3l, "Ket qua thi lai", "Chuc may man lan sau", System.currentTimeMillis()));
    }
}
