package com.example.dzumi.myapplication.servert3h.model;

/**
 * Created by dzumi on 26/01/2015.
 */
public class Email {
    public final static String ID = "Id";
    public final static String TITLE = "title";
    public final static String DESCRIPTION = "description";
    public final static String DATE = "date";

    long id;
    String title;
    String description;
    long date;

    public Email(long id, String title, String description, long date) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.date = date;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }
}
