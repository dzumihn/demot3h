package dzumi.demo.mycontentprovide;

import java.util.List;


/**
 * Created by dzumi on 14/06/2015.
 */
public interface ICountryDAO {
    List<Country> get();
    Country get(int id);

    long insert(Country country);
    int delete(int id);
    int update(int id, String nameEn);
}
