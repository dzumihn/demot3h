package dzumi.demo.mycontentprovide;

import android.content.ContentResolver;
import android.content.Context;


/**
 * Created by dzumi on 01/06/2015.
 */
public class BaseDAO {
    Context mContext;
    ContentResolver mContentResolver;
    public BaseDAO(Context context)
    {
        mContext = context;
        mContentResolver = mContext.getContentResolver();
    }

}