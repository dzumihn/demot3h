package dzumi.demo.mycontentprovide;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by dzumi on 01/06/2015.
 */
public class CountryDAO extends BaseDAO implements ICountryDAO{
    public CountryDAO(Context context)
    {
        super(context);
    }

    public List<Country> get(){
        Cursor cursor = mContentResolver.query(Country.CONTENT_URI, Country.PROJECTION_ALL, null,null, Country.SORT_ORDER_DEFAULT);
        return fetchAll(cursor);
    }

    @Override
    public Country get(int id) {
//        Cursor cursor = mContentResolver.query(Country.)
        return null;
    }

    @Override
    public long insert(Country country) {
        Uri uri = mContentResolver.insert(Country.CONTENT_URI, getContentValues(country));
        return uri != null ? Long.parseLong(uri.getLastPathSegment()) : 0l ;
    }

    @Override
    public int delete(int id) {
        return mContentResolver.delete(Country.CONTENT_URI, Country._ID + "=" + id, null);
    }


    @Override
    public int update(int id, String nameEn) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Country.NAME_EN, nameEn);
       return  mContentResolver.update(Country.CONTENT_URI,contentValues, Country._ID + "=" + id, null);
    }

    private ContentValues getContentValues(Country country)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Country.CAPTION, country.getCaption());
        contentValues.put(Country.FLAG, country.getFlag());
        contentValues.put(Country.NAME_EN, country.getNameEn());
        contentValues.put(Country.NAME_VI, country.getNameVi());
        contentValues.put(Country.POPULATION, country.getPopulation());
        return contentValues;
    }
    List<Country> fetchAll(Cursor cursor)
    {
        List<Country> countries  = new ArrayList<>();
        if(cursor.moveToFirst())
        {
            do{
                countries.add(fetch(cursor));
            }while (cursor.moveToNext());
        }
        cursor.close();
        return countries;
    }

    Country fetch(Cursor cursor)
    {
        Country country = new Country();
        country.set_id(cursor.getInt(cursor.getColumnIndex(Country._ID)));
        country.setNameEn(cursor.getString(cursor.getColumnIndex(Country.NAME_EN)));

        //chuyen tu ten --> id
//        Resources resources = mContext.getResources();
//        country.setFlagID(resources.getIdentifier(country.getFlag().split(".")[0], "drawable", mContext.getPackageName()));
//        country.setFlagID(resources.getIdentifier(country.getFlag().substring(0, country.getFlag().length() - 4), "drawable", context.getPackageName()));

        return country;
    }

}
