package dzumi.demo.mycontentprovide;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by dzumi on 16/05/2015.
 */
public class Country implements BaseColumns{
    public static final String NAME_EN = "nameEn";
    public static final String NAME_VI = "nameVi";
    public static final String FLAG = "flag";
    public static final String POPULATION = "population";
    public static final String CAPTION = "caption";

    public static final String TABLE_NAME = "Country";
    public static final String TABLE_NAME_ID = "Country/#";
    public static final String CONTENT_TYPE_VAlUE = "/app.dzumi.demot3h_country";

    public static final String AUTHORITY =
            "app.dzumi.provider.demot3h";

    public static final Uri CONTENT_URI_BASE =
            Uri.parse("content://" + AUTHORITY);
    /**
     * The content URI for this table.
     */
    public static final Uri CONTENT_URI =
            Uri.withAppendedPath(CONTENT_URI_BASE, TABLE_NAME);
    /**
     * The mime type of a directory of items.
     */
    public static final String CONTENT_TYPE =
            ContentResolver.CURSOR_DIR_BASE_TYPE + CONTENT_TYPE_VAlUE;
    /**
     * The mime type of a single item.
     */
    public static final String CONTENT_ITEM_TYPE =
            ContentResolver.CURSOR_ITEM_BASE_TYPE + CONTENT_TYPE_VAlUE;
    /**
     * A projection of all columns
     * in the items table.
     */
    public static final String[] PROJECTION_ALL =
            {_ID, NAME_EN, NAME_VI, FLAG, POPULATION, CAPTION};

    /**
     * The default sort order for
     * queries containing NAME fields.
     */
    public static final String SORT_ORDER_DEFAULT = NAME_EN + " ASC";

    int _id;
    String nameEn;
    String nameVi;
    int flagID;
    String flag;
    String caption;

    public long getPopulation() {
        return population;
    }

    public void setPopulation(long population) {
        this.population = population;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    long population;

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameVi() {
        return nameVi;
    }

    public void setNameVi(String nameVi) {
        this.nameVi = nameVi;
    }

    public int getFlagID() {
        return flagID;
    }

    public void setFlagID(int flagID) {
        this.flagID = flagID;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public Country(String nameEn, String nameVi, int flagID) {
        this.nameEn = nameEn;
        this.nameVi = nameVi;
        this.flagID = flagID;
    }

    public Country() {
    }
}
