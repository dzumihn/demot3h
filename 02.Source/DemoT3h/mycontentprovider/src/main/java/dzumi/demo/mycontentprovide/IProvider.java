package dzumi.demo.mycontentprovide;

import java.util.List;


/**
 * Created by dzumi on 01/06/2015.
 */
public interface IProvider {
    List<Country> get();
    long insert(Country country);

}
