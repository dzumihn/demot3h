package dzumi.demo.mycontentprovide;

import android.content.Context;

import java.util.List;


/**
 * Created by dzumi on 01/06/2015.
 */
public class Provider3 implements IProvider {

    CountryDAO countryDAO;
    public Provider3(Context context)
    {
        countryDAO = new CountryDAO(context);
    }
    @Override
    public List<Country> get() {
        return countryDAO.get();
    }

    @Override
    public long insert(Country country) {
        return countryDAO.insert(country);
    }
}
