package app.dzumi.demot3h;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;

/**
 * Created by dzumi on 20/04/2015.
 */
public class TextViewActivity extends ActionBarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second);
        Log.d("dzumi", "onCreate() - " + getClass().getName());

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("dzumi", "onStart() - " + getClass().getName());
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("dzumi", "onResume() - " + getClass().getName());
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("dzumi", "onPause() - " + getClass().getName());
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("dzumi", "onRestart() - " + getClass().getName());
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("dzumi", "onStop() - " + getClass().getName());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("dzumi", "onDestroy() - " + getClass().getName());
    }

}
