package app.dzumi.demot3h;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity implements View.OnClickListener{

    Button btnNext, btnBack, btnExit;
    Button btnTextView;
    TextView tvTitle;
    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int id = view.getId();
            if(id == R.id.btnNext)
            {
                Toast.makeText(MainActivity.this,"Next", Toast.LENGTH_SHORT).show();
            }else if(id == R.id.btnExit)
            {
                Toast.makeText(MainActivity.this,"Exits", Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Log.d("dzumi", "onCreate() - " + getClass().getName());

        //tim kiem view tu xml --> gan cho doi tuong tren java code
        btnBack = (Button) findViewById(R.id.btnBack);
        btnNext = (Button) findViewById(R.id.btnNext);
        btnExit = (Button) findViewById(R.id.btnExit);
        btnTextView = (Button) findViewById(R.id.btnTextView);
        tvTitle = (TextView) findViewById(R.id.tvTitle);

        //bat su kien
        //cach 1
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this,"Back", Toast.LENGTH_SHORT).show();
            }
        });
        //cach 2
        btnNext.setOnClickListener(onClickListener);
        btnExit.setOnClickListener(onClickListener);
        //cach 3
        btnTextView.setOnClickListener(this);
        tvTitle.setText("Man hinh Activity 1");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("dzumi", "onStart() - " + getClass().getName());
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("dzumi", "onResume() - " + getClass().getName());
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("dzumi", "onPause() - " + getClass().getName());
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("dzumi", "onRestart() - " + getClass().getName());
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("dzumi", "onStop() - " + getClass().getName());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("dzumi", "onDestroy() - " + getClass().getName());
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(this,"TextView", Toast.LENGTH_SHORT).show();
        //goi activity TextView
        Intent intent = new Intent(this, TextViewActivity.class);
        startActivity(intent);
    }


}
