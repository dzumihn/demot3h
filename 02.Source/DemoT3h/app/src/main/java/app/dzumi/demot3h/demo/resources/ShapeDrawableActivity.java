package app.dzumi.demot3h.demo.resources;

import android.app.Fragment;
import android.view.View;

import app.dzumi.demot3h.BaseActivity;
import app.dzumi.demot3h.R;

/**
 * Created by dzumi on 16/05/2015.
 */
public class ShapeDrawableActivity extends BaseActivity {
    @Override
    protected Fragment initFragment() {
        return null;
    }

    @Override
    protected View initContentView() {
        return getView(R.layout.activity_resource_drawable_shape);
    }
}
