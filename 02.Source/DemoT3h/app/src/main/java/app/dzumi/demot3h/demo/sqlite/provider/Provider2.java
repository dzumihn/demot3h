package app.dzumi.demot3h.demo.sqlite.provider;

import android.content.Context;

import java.util.List;

import app.dzumi.demot3h.demo.sqlite.dao.CountryDAO2;
import app.dzumi.demot3h.demo.sqlite.model.Country;

/**
 * Created by dzumi on 01/06/2015.
 */
public class Provider2 implements IProvider {

    CountryDAO2 countryDAO;
    public Provider2(Context context)
    {
        countryDAO = new CountryDAO2(context);
    }
    @Override
    public List<Country> get() {
        return countryDAO.get();
    }

    @Override
    public long insert(Country country) {
        return 0;
    }
}
