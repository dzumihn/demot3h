package app.dzumi.demot3h.demo.intent;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;

import app.dzumi.demot3h.BaseActivity;
import app.dzumi.demot3h.R;

/**
 * Created by dzumi on 29/05/2015.
 */
public class ActivitySampleIntent1 extends BaseActivity implements View.OnClickListener {
    @Override
    protected Fragment initFragment() {
        return null;
    }

    @Override
    protected View initContentView() {
        return getView(R.layout.activity_sample_intent);
    }

    Button btnCamera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        btnCamera = (Button) findViewById(R.id.btnCamera);
        btnCamera.setOnClickListener(this);
    }

    public final static int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1;
    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btnCamera) {
            // create Intent to take a picture and return control to the calling application
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File file  = new File(Environment.getExternalStorageDirectory()
                    + "/Download/capture_" + System.currentTimeMillis() + ".jpg");
            Uri fileUri = Uri.fromFile(file); // create a file to save the image
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file name

            // start the image capture Intent
            startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE)
        {
            if(resultCode == RESULT_OK)//thanh cong
            {
                Toast.makeText(this, "OK", Toast.LENGTH_SHORT).show();
            }
            else if(resultCode == RESULT_CANCELED)//huy
            {
                Toast.makeText(this, "Cancel", Toast.LENGTH_SHORT).show();
            }
        }
    }

}
