package app.dzumi.demot3h.demo.notification;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import app.dzumi.demot3h.R;

/**
 * Demo Notification
 *
 */
public class MainNotificationActivity extends ActionBarActivity {

    public static String[] listUrls = {"http://vnexpress.net/tin-tuc/thoi-su/giao-thong/de-xuat-tich-thu-phuong-tien-neu-lai-xe-co-nong-do-con-cao-3152970.html",
    "http://doisong.vnexpress.net/photo/nhip-song/nhung-dua-tre-trong-chien-tranh-viet-nam-sau-nua-the-ky-3152476.html",
    "http://kinhdoanh.vnexpress.net/tin-tuc/doanh-nghiep/tiep-vien-phi-cong-vietnam-airlines-doi-dong-phuc-3152211.html"};

    public static String[] listNumbers = {"1","2", "3"};

    public final static String KEY_URL = "KEY_URL";
    Button btnGo;
    Spinner spinner;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_notification);

        context = this;
        btnGo  = (Button) findViewById(R.id.btnGo);
        btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainNotificationActivity.this, ResultNotificationActivity.class);
                int pos = Integer.parseInt(spinner.getSelectedItem().toString())-1;
                intent.putExtra(KEY_URL, listUrls[pos]);
                startActivity(intent);

            }
        });

        spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listNumbers);
        spinner.setAdapter(adapter);

    }


    @Override
    protected void onStop() {
        super.onStop();
        Log.d("dzumi", "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("dzumi", "onDestroy");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("dzumi", "onResume");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("dzumi", "onStart");
    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.d("dzumi", "onPause");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_notification, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
