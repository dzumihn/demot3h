package app.dzumi.demot3h.demo.sqlite.contentprovider.provider;

import android.content.Context;

import java.util.List;

import app.dzumi.demot3h.demo.sqlite.contentprovider.base.Country;
import app.dzumi.demot3h.demo.sqlite.contentprovider.dao.CountryDAOContentProvider;


/**
 * Created by dzumi on 01/06/2015.
 */
public class Provider3 implements IProvider {

    CountryDAOContentProvider countryDAO;
    public Provider3(Context context)
    {
        countryDAO = new CountryDAOContentProvider(context);
    }
    @Override
    public List<Country> get() {
        return countryDAO.get();
    }

    @Override
    public long insert(Country country) {
        return countryDAO.insert(country);
    }
}
