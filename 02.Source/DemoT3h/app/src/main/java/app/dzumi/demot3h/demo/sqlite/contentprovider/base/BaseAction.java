package app.dzumi.demot3h.demo.sqlite.contentprovider.base;

import android.database.Cursor;

public interface BaseAction {
    void clearAllData();
    Cursor rawQuery(String sql, String[] args);
}