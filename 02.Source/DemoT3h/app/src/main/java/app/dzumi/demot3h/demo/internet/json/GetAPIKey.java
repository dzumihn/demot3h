package app.dzumi.demot3h.demo.internet.json;

import android.content.Context;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import app.dzumi.demot3h.R;
import app.dzumi.demot3h.main.utils.Log;
import app.dzumi.demot3h.main.utils.network.HttpHelper;
import app.dzumi.demot3h.main.utils.network.HttpHelperRequest;


/**
 * Created by duynguyen on 3/26/15.
 */
public class GetAPIKey extends HttpHelperRequest {


    Context mContext;
    OnCompletedGetAPIKey mOnCompletedGetAPIKey;
    private final String TAG = "GetAPIKey";

    private GetAPIKey(Context context,
                      OnCompletedGetAPIKey onCompletedGetAPIKey)
    {
        mContext = context;
        mOnCompletedGetAPIKey = onCompletedGetAPIKey;
    }



    public static HttpHelperRequest getInstance(Context context, OnCompletedGetAPIKey onCompletedGetAPIKey) {
        return new GetAPIKey(context, onCompletedGetAPIKey);
    }

    @Override
    protected void onResponseListener(JSONObject jsonResponse) {
        if (!jsonResponse.has("ApiKey")) {
            mOnCompletedGetAPIKey.onCompletedGetAPIKey(false, mContext.getString(R.string.dialog_error_system));
            return;
        }
        try {
            getKey(jsonResponse);
        } catch (UnsupportedEncodingException e) {
            mOnCompletedGetAPIKey.onCompletedGetAPIKey(false, mContext.getString(R.string.dialog_error_system));
        } catch (JSONException e) {
            mOnCompletedGetAPIKey.onCompletedGetAPIKey(false, mContext.getString(R.string.dialog_error_system));
        }
    }

    @Override
    public String getTAG() {
        return null;
    }

    @Override
    public void post() {
        HttpHelper httpHelper = HttpHelper.getInstance(mContext);
        httpHelper.makeJsonRequest(
                HttpHelper.Method.GET,
                "http://logical-river-834.appspot.com/api/getAPIKey", // url
                new JSONObject(), // request params
                new HttpHelper.ResponseHandler() {
                    @Override
                    public void handleJsonResponse(JSONObject jsonObject) {
//                            if(!jsonObject.has("Result")) {
                        onResponseListener(jsonObject);
                    }
                }, // handle response
                new HttpHelper.ResponseErrorHandler() {

                    @Override
                    public void handleError(VolleyError error, String msg) {
                        mOnCompletedGetAPIKey.onCompletedGetAPIKey(false,msg);
                    }
                }, // handle exception
                null, // tag
                HttpHelper.TIMEOUT, // timeout
                HttpHelper.RETRY_TIMES); // retry times
    }

    private void getKey(JSONObject jsonObject) throws UnsupportedEncodingException, JSONException {
        Log.d("API",jsonObject.getString("APIKey"));
        mOnCompletedGetAPIKey.onCompletedGetAPIKey(true);
    }

    public interface OnCompletedGetAPIKey{
        void onCompletedGetAPIKey(boolean isOK, String... error);
    }
}
