package app.dzumi.demot3h.demo.sqlite.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import app.dzumi.demot3h.demo.sqlite.base.DBContext;

/**
 * Created by dzumi on 01/06/2015.
 */
public class BaseDAO {
    protected DBContext db;
    protected Context context;

    public Cursor loadAll(String table,String[] columns) {
        return db.getReadableDatabase().query(table,columns,null,null,null,null,null);
    }

    public void setContext(Context context) {
        db = new DBContext(context);
        this.context = context;
    }

    public Cursor loadAll(String table, String[] columns, String selection, String[] args) {
        return db.getReadableDatabase().query(table,columns,selection,args,null,null,null);
    }

    public Cursor loadAll(String table, String[] columns, String selection, String[] args, String orderBy) {
        return db.getReadableDatabase().query(table,columns,selection,args,null,null,orderBy);
    }

    public long insert(String table, ContentValues contentValues) {

        return db.getWritableDatabase().insert(table, null, contentValues);
    }

    public long insertNew(String table, ContentValues contentValues) {

        return db.getWritableDatabase().insert(table, null, contentValues);
    }

    public int update(String table, ContentValues contentValues,String selection, String[] args) {
        return db.getWritableDatabase().update(table, contentValues, selection, args);
    }

    public int delete(String table, String whereClause, String[] whereArgs)
    {
        return db.getWritableDatabase().delete(table, whereClause, whereArgs);
    }

    public void executeSQL(String strSql) {
        db.getWritableDatabase().execSQL(strSql);
    }

    public Cursor executeSQLResult(String strSql, String[] args) {
        return db.getReadableDatabase().rawQuery(strSql, args);
    }


    /**
     * @Function name: clearTable
     * @Description clear table
     * @param table
     *            Table need to clear
     */
    private void clearTable(SQLiteDatabase database, String table) {
        // Clear all record
        // Truncate
        database.execSQL("DELETE FROM " + table);
    }
}