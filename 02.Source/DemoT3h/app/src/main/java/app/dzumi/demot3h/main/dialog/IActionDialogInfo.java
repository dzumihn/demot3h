package app.dzumi.demot3h.main.dialog;

/**
 * Created by dzumi on 30/05/2015.
 */
public interface IActionDialogInfo {
    void onPositiveButtonClick();
    void onNegativeButtonClick();
}
