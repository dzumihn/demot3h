package app.dzumi.demot3h.main.utils;

/**
 * Created by dzumi on 08/03/2015.
 */
public class Log {

    private final static String TAG_NAME = "dzumi";
    public static void d(String log)
    {
        android.util.Log.d(TAG_NAME, log);
    }
    public static void d(String tag, String log)
    {
        android.util.Log.d(tag, log);
    }
    public static void e(String log)
    {
        android.util.Log.e(TAG_NAME, log);
    }
}
