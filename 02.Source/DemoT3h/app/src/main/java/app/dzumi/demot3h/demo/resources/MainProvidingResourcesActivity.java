/*
* Copyright 2013 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package app.dzumi.demot3h.demo.resources;

import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import app.dzumi.demot3h.MainPagerActivity;
import app.dzumi.demot3h.R;
import app.dzumi.demot3h.main.fragment.ListViewFragment;
import app.dzumi.demot3h.main.fragment.SlidingTabsFragment;
import app.dzumi.demot3h.main.fragment.SlidingTabsFragment.PagerItem;

/**
 * A simple launcher activity containing a summary sample description, sample log and a custom
 * {@link Fragment} which can display a view.
 * <p>
 * For devices with displays with a width of 720dp or greater, the sample log is always visible,
 * on other devices it's visibility is controlled by an item on the Action Bar.
 */
public class MainProvidingResourcesActivity extends MainPagerActivity {

    public static final String TAG = "MainProvidingResourcesActivity";

    // Whether the Log Fragment is currently shown
    private boolean mLogShown;
    private List<PagerItem> mTabs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main_tab_pager);
        if (savedInstanceState == null) {
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            mTabs = new ArrayList<>();
            initData();

            SlidingTabsFragment fragment = new SlidingTabsFragment();
            transaction.replace(R.id.sample_content_fragment, fragment);
            transaction.commit();
        }
    }

    void initData()
    {
        //<editor-fold desc="mcc, mnc">
        mTabs.add(new PagerItem(
                getString(R.string.title_mcc_mnc_demo), // Title
                Color.BLUE, // Indicator color
                Color.GRAY // Divider color
        ));

        Configuration configuration = getResources().getConfiguration();
        ArrayList<String> itemMCC_MNC = new ArrayList<>();
        itemMCC_MNC.add("mcc: " + configuration.mcc);
        itemMCC_MNC.add("mnc: " + configuration.mnc);
        ListViewFragment fragmentMcc_Mnc = ListViewFragment.newInstance(itemMCC_MNC);

        mTabs.get(0).setFragment(fragmentMcc_Mnc);
        //</editor-fold>

        //<editor-fold desc="lang-region">
        mTabs.add(new PagerItem(
                getString(R.string.title_lang_reg_demo), // Title
                Color.BLUE, // Indicator color
                Color.GRAY // Divider color
        ));
        ArrayList<String> itemLangRegion = new ArrayList<>();

        itemLangRegion.add(getString(R.string.double_quotation_mark));
        itemLangRegion.add(getString(R.string.single_quotation_mark));
        itemLangRegion.add(getString(R.string.html_tag));
        ListViewFragment fragmentLang_Reg = ListViewFragment.newInstance(itemLangRegion);
        mTabs.get(1).setFragment(fragmentLang_Reg);
        //</editor-fold>

        //<editor-fold desc="layout direction">
        mTabs.add(new PagerItem(
                getString(R.string.title_lay_dir_demo), // Title
                Color.BLUE, // Indicator color
                Color.GRAY // Divider color
        ));
        ArrayList<String> itemLayoutDirection = new ArrayList<>();
        int layoutDir = configuration.getLayoutDirection(); //API 17
        switch (layoutDir) {
            case View.LAYOUT_DIRECTION_LTR:
                itemLayoutDirection.add("Left to Right: " + layoutDir);
                break;
            case View.LAYOUT_DIRECTION_RTL:
                itemLayoutDirection.add("Right to Left: " + layoutDir);
                break;
            default:
                itemLayoutDirection.add("default: " + layoutDir);
        }
        ListViewFragment fragmentLayoutDirection = ListViewFragment.newInstance(itemLayoutDirection);
        mTabs.get(2).setFragment(fragmentLayoutDirection);
        //</editor-fold>

        //<editor-fold desc="smallest width">
        mTabs.add(new PagerItem(
                getString(R.string.title_smallest_width_demo), // Title
                Color.BLUE, // Indicator color
                Color.GRAY // Divider color
        ));
        ArrayList<String> itemSmallestWidth = new ArrayList<>();
        itemSmallestWidth.add("Smallest Width: " + configuration.smallestScreenWidthDp);
        ListViewFragment fragmentSmallestWidth = ListViewFragment.newInstance(itemSmallestWidth);
        mTabs.get(3).setFragment(fragmentSmallestWidth);
        //</editor-fold>

        //<editor-fold desc="screen size">
        mTabs.add(new PagerItem(
                getString(R.string.title_scr_size_demo), // Title
                Color.BLUE, // Indicator color
                Color.GRAY // Divider color
        ));
        ArrayList<String> itemScrSize = new ArrayList<>();

        int scrSize = configuration.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK;

        switch (scrSize) {
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                itemScrSize.add("Screen size: normal - " + configuration.screenLayout);
                break;
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                itemScrSize.add("Screen size: large - " + configuration.screenLayout);
                break;
            case Configuration.SCREENLAYOUT_SIZE_SMALL:
                itemScrSize.add("Screen size: small - " + configuration.screenLayout);
                break;
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
                itemScrSize.add("Screen size: xlarge - " + configuration.screenLayout);
                break;
            default:
                itemScrSize.add("Screen size: undefine - " + configuration.screenLayout);
        }
        ListViewFragment fragmentScreenSize = ListViewFragment.newInstance(itemScrSize);
        mTabs.get(4).setFragment(fragmentScreenSize);
        //</editor-fold>

        //<editor-fold desc="screen aspect">
        mTabs.add(new PagerItem(
                getString(R.string.title_scr_aspect_demo), // Title
                Color.BLUE, // Indicator color
                Color.GRAY // Divider color
        ));
        ArrayList<String> itemScrAspect = new ArrayList<>();
        int scrLong = configuration.screenLayout & Configuration.SCREENLAYOUT_LONG_MASK;

        switch (scrLong) {
            case Configuration.SCREENLAYOUT_LONG_NO:
                itemScrAspect.add("Long: no - " + configuration.screenLayout);
                break;
            case Configuration.SCREENLAYOUT_LONG_YES:
                itemScrAspect.add("Long: yes - " + configuration.screenLayout);
                break;
            default:
                itemScrSize.add("long: undefine - " + configuration.screenLayout);
        }
        ListViewFragment fragmentScreenAspect = ListViewFragment.newInstance(itemScrAspect);
        mTabs.get(5).setFragment(fragmentScreenAspect);
        //</editor-fold>

        //<editor-fold desc="screen orientation">
        mTabs.add(new PagerItem(
                getString(R.string.title_scr_orientation_demo), // Title
                Color.BLUE, // Indicator color
                Color.GRAY // Divider color
        ));
        ArrayList<String> itemOrientation = new ArrayList<>();

        switch (configuration.orientation) {
            case Configuration.ORIENTATION_LANDSCAPE:
                itemOrientation.add("ORIENTATION_LANDSCAPE - " + Configuration.ORIENTATION_LANDSCAPE);
                break;
            case Configuration.ORIENTATION_PORTRAIT:
                itemOrientation.add("ORIENTATION_PORTRAIT - " + Configuration.ORIENTATION_PORTRAIT);
                break;
            default:
                itemScrSize.add("long: undefine - " + configuration.screenLayout);
        }
        ListViewFragment fragmentScreenOrientation = ListViewFragment.newInstance(itemOrientation);
        mTabs.get(6).setFragment(fragmentScreenOrientation);
        //</editor-fold>

        //<editor-fold desc="ui mode">
        mTabs.add(new PagerItem(
                getString(R.string.title_ui_mode_demo), // Title
                Color.BLUE, // Indicator color
                Color.GRAY // Divider color
        ));
        ArrayList<String> itemUIMode = new ArrayList<>();
        int uiMode = configuration.uiMode & Configuration.UI_MODE_TYPE_MASK;

        switch (uiMode) {
            case Configuration.UI_MODE_TYPE_APPLIANCE:
                itemUIMode.add("UI_MODE_TYPE_APPLIANCE - " + Configuration.UI_MODE_TYPE_APPLIANCE);
                break;
            case Configuration.UI_MODE_TYPE_CAR:
                itemUIMode.add("UI_MODE_TYPE_CAR - " + Configuration.UI_MODE_TYPE_CAR);
                break;
            case Configuration.UI_MODE_TYPE_DESK:
                itemUIMode.add("UI_MODE_TYPE_DESK - " + Configuration.UI_MODE_TYPE_DESK);
                break;
            case Configuration.UI_MODE_TYPE_NORMAL:
                itemUIMode.add("UI_MODE_TYPE_NORMAL - " + Configuration.UI_MODE_TYPE_NORMAL);
                break;
            case Configuration.UI_MODE_TYPE_TELEVISION:
                itemUIMode.add("UI_MODE_TYPE_TELEVISION - " + Configuration.UI_MODE_TYPE_TELEVISION);
                break;
            case Configuration.UI_MODE_TYPE_WATCH:
                itemUIMode.add("UI_MODE_TYPE_WATCH - " + Configuration.UI_MODE_TYPE_WATCH);
                break;
            default:
                itemUIMode.add("UI_MODE_TYPE_UNDEFINED - " + Configuration.UI_MODE_TYPE_UNDEFINED);
        }
        ListViewFragment fragmentUIMode = ListViewFragment.newInstance(itemUIMode);
        mTabs.get(7).setFragment(fragmentUIMode);
        //</editor-fold>

        //<editor-fold desc="night mode">
        mTabs.add(new PagerItem(
                getString(R.string.title_night_mode_demo), // Title
                Color.BLUE, // Indicator color
                Color.GRAY // Divider color
        ));
        ArrayList<String> itemNightMode = new ArrayList<>();
        int nightMode = configuration.uiMode & Configuration.UI_MODE_NIGHT_MASK;

        switch (nightMode) {
            case Configuration.UI_MODE_NIGHT_NO:
                itemNightMode.add("UI_MODE_NIGHT_NO - " + Configuration.UI_MODE_NIGHT_NO);
                break;
            case Configuration.UI_MODE_NIGHT_YES:
                itemNightMode.add("UI_MODE_NIGHT_YES - " + Configuration.UI_MODE_NIGHT_YES);
                break;
            default:
                itemNightMode.add("UI_MODE_NIGHT_UNDEFINED - " + Configuration.UI_MODE_NIGHT_UNDEFINED);
        }
        ListViewFragment fragmentNightMode = ListViewFragment.newInstance(itemNightMode);
        mTabs.get(8).setFragment(fragmentNightMode);
        //</editor-fold>

        //<editor-fold desc="Screen pixel density (dpi)">
        mTabs.add(new PagerItem(
                getString(R.string.title_screen_density_demo), // Title
                Color.BLUE, // Indicator color
                Color.GRAY // Divider color
        ));

        ArrayList<String> itemScreenDensity = new ArrayList<>();
        itemScreenDensity.add("Screen pixel density: " + configuration.densityDpi);
        ListViewFragment fragmentScreenDensity = ListViewFragment.newInstance(itemScreenDensity);

        mTabs.get(9).setFragment(fragmentScreenDensity);
        //</editor-fold>
    }

}
