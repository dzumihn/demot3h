package app.dzumi.demot3h.demo.internet.json;

import android.content.Context;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import app.dzumi.demot3h.main.utils.Log;
import app.dzumi.demot3h.main.utils.network.HttpHelper;
import app.dzumi.demot3h.main.utils.network.HttpHelperRequest;


/**
 * Created by duynguyen on 3/26/15.
 */
public class Authenticate extends HttpHelperRequest {


    JSONObject mJsonRequest;
    Context mContext;
    OnCompleteAuthenticate mOnCompleteAuthenticate;
    private final String TAG = "Authenticate";

    private Authenticate(JSONObject jsonRequest, Context context, OnCompleteAuthenticate onCompleteAuthenticate)
    {
        mJsonRequest = jsonRequest;
        mContext = context;
        mOnCompleteAuthenticate = onCompleteAuthenticate;
    }

    public static JSONObject createJsonRequest(Context context, String userID, String password)
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_name", userID);
            jsonObject.put("pass", password);

            jsonObject.put("APIKey", "AfkxyA20fjAOiyfu");

        } catch (JSONException e) {
            e.printStackTrace();
        }

//        jsonObject.put()
        return jsonObject;
    }

    public static HttpHelperRequest getInstance(JSONObject jsonRequest, Context context, OnCompleteAuthenticate onCompleteAuthenticate) {
        return new Authenticate(jsonRequest, context, onCompleteAuthenticate);
    }

    @Override
    protected void onResponseListener(JSONObject jsonResponse) {
        try {
            //loi
            if (jsonResponse.getInt("status") == 0)
            {
                mOnCompleteAuthenticate.onCompleteAuthenticate(false, jsonResponse.getString("error_message"));
                return;
            }
            validate(jsonResponse);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getTAG() {
        return null;
    }

    @Override
    public void post() {
        HttpHelper httpHelper = HttpHelper.getInstance(mContext);
        httpHelper.makeJsonRequest(
                HttpHelper.Method.POST,
//                "http://logical-river-834.appspot.com/api/login", // url
                "http://localhost:8080//api/login", // url
                mJsonRequest, // request params
                new HttpHelper.ResponseHandler() {
                    @Override
                    public void handleJsonResponse(JSONObject jsonObject) {
                       onResponseListener(jsonObject);
                    }
                }, // handle response
                new HttpHelper.ResponseErrorHandler() {

                    @Override
                    public void handleError(VolleyError error, String msg) {
                        mOnCompleteAuthenticate.onCompleteAuthenticate(false, msg);
                    }
                }, // handle exception
                null, // tag
                HttpHelper.TIMEOUT, // timeout
                HttpHelper.RETRY_TIMES // retry times
        );
    }

    private void validate(JSONObject jsonObject) throws JSONException {
        Log.d("API", jsonObject.getString("error_message"));
    }

    public interface OnCompleteAuthenticate{
        void onCompleteAuthenticate(boolean isOK, String... error);
    }
}
