package app.dzumi.demot3h.demo.services;

import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import app.dzumi.demot3h.main.utils.Log;

/**
 * Created by dzumi on 13/07/2015.
 */
public class SampleService1 extends BaseServices {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("onStartCommand" + this.getClass().toString());
        return START_NOT_STICKY;
    }


}
