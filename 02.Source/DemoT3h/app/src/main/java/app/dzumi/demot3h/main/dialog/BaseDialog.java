package app.dzumi.demot3h.main.dialog;

import android.app.DialogFragment;

/**
 * Created by duynguyen on 4/8/15.
 */
public class BaseDialog extends DialogFragment{
    public static final String DIALOG_TITLE = "title";
    public static final String DIALOG_CONTENT = "content";
    public static final String DIALOG_NOTICE = "notice";
    public static final String DIALOG_TYPE = "dialog_type";
    public static final String DIALOG_BUNDLE = "bundle";
    public static final int DIALOG_TYPE_YES_NO = 1;
    public static final int DIALOG_TYPE_OK = 2;
    public static final String DIALOG_OK_HAVE_EVENT = "dialog_ok_have_event";//truong hop dialog ok muon send su kien cho fragment
    //key intent form dialogInfo to fragment
    public static final String RESULT_INFO = "result_info";

}
