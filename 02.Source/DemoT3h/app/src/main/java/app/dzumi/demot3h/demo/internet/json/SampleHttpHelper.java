package app.dzumi.demot3h.demo.internet.json;

import android.content.Context;
import android.content.SharedPreferences;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import app.dzumi.demot3h.main.utils.network.HttpHelper;
import app.dzumi.demot3h.main.utils.network.HttpHelperRequest;

/**
 * Created by dzumi on 24/07/2015.
 */
public class SampleHttpHelper extends HttpHelperRequest {

    OnCompleted mCompleted;
    Context mContext;
    String mLink;
    public SampleHttpHelper(Context context, String link, OnCompleted onCompleted)
    {
        mContext = context;
        mCompleted = onCompleted;
        mLink = link;
    }

    //phuong thuc dc goi khi nhan dc ket qua tu server
    //chuyen jsonResp --> activity
    @Override
    protected void onResponseListener(JSONObject jsonResponse) {
        //xu ly du lieu --> luu tru ...
        SharedPreferences.Editor editor = mContext.getSharedPreferences("pref", Context.MODE_PRIVATE).edit();
        try {
            editor.putString("APIKey", jsonResponse.getString("APIKey"));
            editor.commit();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mCompleted.onCompleted(true);
    }

    @Override
    public String getTAG() {
        return null;
    }

    @Override
    public void post() {
        HttpHelper httpHelper = HttpHelper.getInstance(mContext);
        httpHelper.makeJsonRequest(
                HttpHelper.Method.GET,
                mLink, new JSONObject(), new HttpHelper.ResponseHandler() {
                    @Override
                    public void handleJsonResponse(JSONObject jsonObject) {
                        //xu ly khi thanh cong
                        onResponseListener(jsonObject);
                    }
                }, new HttpHelper.ResponseErrorHandler() {
                    @Override
                    public void handleError(VolleyError error, String msg) {
                        //xu ly khi lay du lieu tu server that bai
                        mCompleted.onCompleted(false,msg);
                    }
                },"",500,0
        );
    }

    public interface OnCompleted{
        void onCompleted(boolean isOK, String ... errors);
    }
}
