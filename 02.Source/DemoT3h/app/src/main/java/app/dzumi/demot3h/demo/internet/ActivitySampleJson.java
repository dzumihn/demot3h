package app.dzumi.demot3h.demo.internet;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import app.dzumi.demot3h.BaseActivity;
import app.dzumi.demot3h.R;
import app.dzumi.demot3h.demo.internet.json.Authenticate.OnCompleteAuthenticate;
import app.dzumi.demot3h.demo.internet.json.AuthenticateBasic;
import app.dzumi.demot3h.demo.internet.json.GetAPIKey;
import app.dzumi.demot3h.demo.internet.json.SampleHttpHelper;
import app.dzumi.demot3h.main.utils.network.HttpHelperRequest;

/**
 * Created by dzumi on 23/07/2015.
 */
public class ActivitySampleJson extends BaseActivity implements
        View.OnClickListener,
        GetAPIKey.OnCompletedGetAPIKey, OnCompleteAuthenticate,
        SampleHttpHelper.OnCompleted{
    @Override
    protected Fragment initFragment() {
        return null;
    }

    @Override
    protected View initContentView() {
        return getView(R.layout.activity_sample_json);
    }

    Button btnLogin, btnGetAPIKey;
    EditText edtAPIKey, edtUsername, edtPass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnGetAPIKey = (Button) findViewById(R.id.btnGetAPIKey);

        edtAPIKey = (EditText) findViewById(R.id.edtAPIKey);
        edtUsername = (EditText) findViewById(R.id.edtUserName);
        edtPass = (EditText) findViewById(R.id.edtPass);

        btnGetAPIKey.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if(id == R.id.btnGetAPIKey)
        {
            /*HttpHelperRequest request = GetAPIKey.getInstance(this,this );
            request.post();*/
          /*  GetAPIKeyBasic basic = new GetAPIKeyBasic();
            basic.execute("http://logical-river-834.appspot.com/api/getAPIKey");*/
            HttpHelperRequest request = new SampleHttpHelper(this,"http://logical-river-834.appspot.com/api/getAPIKey",this);
            request.post();
        }else if(id == R.id.btnLogin)
        {
           /* HttpHelperRequest request =
                    Authenticate.getInstance(
                            Authenticate.createJsonRequest(this,
                                    edtUsername.getText().toString(),
                                    edtPass.getText().toString()),
                    this, this);
            request.post();*/
            AuthenticateBasic authenticateBasic = new AuthenticateBasic(edtUsername.getText().toString(),
                    edtAPIKey.getText().toString(),
                    edtPass.getText().toString());
            authenticateBasic.execute("http://logical-river-834.appspot.com/api/login");
        }
    }

    @Override
    public void onCompletedGetAPIKey(boolean isOK, String... error) {
        //hien thi len giao dien ket qua nhan duoc tu server
        if(isOK) //lay ket qua thanh cong
        {
            //lay APIKey tu sharePref load len editText
            SharedPreferences sharedPreferences = getSharedPreferences("pref", MODE_PRIVATE);
            edtAPIKey.setText(sharedPreferences.getString("APIKey", "Default"));
        }else {
            //that bai --> ly do tai sao? --> error
            Toast.makeText(this, error[0], Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onCompleteAuthenticate(boolean isOK, String... error) {
        if(isOK)
        {
            //
        }
        else
        {
            Toast.makeText(this,error[0], Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onCompleted(boolean isOK, String... errors) {
        //hien thi len giao dien ket qua nhan duoc tu server
        if(isOK) //lay ket qua thanh cong
        {
            //lay APIKey tu sharePref load len editText
            SharedPreferences sharedPreferences = getSharedPreferences("pref", MODE_PRIVATE);
            edtAPIKey.setText(sharedPreferences.getString("APIKey", "Default"));
        }else {
            //that bai --> ly do tai sao? --> error
            Toast.makeText(this, errors[0], Toast.LENGTH_SHORT).show();
        }
    }
}
