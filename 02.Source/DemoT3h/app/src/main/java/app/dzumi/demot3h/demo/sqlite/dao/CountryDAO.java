package app.dzumi.demot3h.demo.sqlite.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import app.dzumi.demot3h.demo.sqlite.model.Country;

/**
 * Created by dzumi on 01/06/2015.
 */
public class CountryDAO {
    SQLiteDatabase database;
   /* String path = Environment.getExternalStorageDirectory() +
            "/Download/DBCountry";*/
    public CountryDAO(Context context)
    {
//        database = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.OPEN_READWRITE);
    }

    public CountryDAO(Context context, String path)
    {
        database = context.openOrCreateDatabase(path, Context.MODE_PRIVATE, null);
    }

    public List<Country> get(){
        Cursor cursor = database.query(Country.TABLE_NAME, null, null, null, null, null, null);
        return fetchAll(cursor);
    }

    List<Country> fetchAll(Cursor cursor)
    {
        List<Country> countries  = new ArrayList<>();
        if(cursor.moveToFirst())
        {
            do{
                countries.add(fetch(cursor));
            }while (cursor.moveToNext());
        }
        cursor.close();
//        database.close();
        return countries;
    }

    Country fetch(Cursor cursor)
    {
        Country country = new Country();
        country.set_id(cursor.getInt(cursor.getColumnIndex(Country._ID)));
        country.setNameEn(cursor.getString(cursor.getColumnIndex(Country.NAME_EN)));
        return country;
    }

    public long insertCountry(Country country)
    {
        return database.insert(Country.TABLE_NAME, null, getContentValues(country));
    }

    public int updateCountry(long id, String nameEN)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Country.NAME_EN, nameEN);
        return database.update(Country.TABLE_NAME, contentValues, Country._ID + " =?", new String[]{nameEN});
    }

    int deleteCountry(long id)
    {
        return database.delete(Country.TABLE_NAME, Country._ID + "=?", new String[]{id+ ""});
    }

    public int deleteCountry(long populationFrom, long populationTo )
    {
        return database.delete(Country.TABLE_NAME,
                Country.POPULATION + " >= ? and " + Country.POPULATION + " <= ?",
                new String[]{populationFrom+ "" , populationTo + ""});
    }

    private ContentValues getContentValues(Country country)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Country.CAPTION, country.getCaption());
        contentValues.put(Country.FLAG, country.getFlag());
        contentValues.put(Country.NAME_EN, country.getNameEn());
        contentValues.put(Country.NAME_VI, country.getNameVi());
        contentValues.put(Country.POPULATION, country.getPopulation());
        return contentValues;
    }
    public void closeDB()
    {
        database.close();
    }
}
