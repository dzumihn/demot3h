package app.dzumi.demot3h.main.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import app.dzumi.demot3h.R;
import app.dzumi.demot3h.main.interfaces.OnPagerChanged;

/**
 * Created by dzumi on 10/05/2015.
 */
public class MainPagerFragment extends BaseFragment implements OnPagerChanged {

    SlidingTabsFragment mSlidingTabsFragment;
    private TextView tvLog;

    @Override
    protected int layoutID() {
        return R.layout.activity_main_tab_pager;
    }

    @Override
    protected void initViews(View root, Bundle savedInstanceState) {
        tvLog = findView(R.id.tvLog);
    }

    @Override
    protected void setupViews() {

    }

    @Override
    public String getTagFragment() {
        return null;
    }

    @Override
    public void onPagerChanged(String log) {
        tvLog.setText(log);
    }
}
