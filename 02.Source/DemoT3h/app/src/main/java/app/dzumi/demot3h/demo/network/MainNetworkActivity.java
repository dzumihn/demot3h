package app.dzumi.demot3h.demo.network;

import android.app.Fragment;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;

import app.dzumi.demot3h.BaseActivity;
import app.dzumi.demot3h.R;

public class MainNetworkActivity extends BaseActivity {

    Button btnCheckNetwork;
    TextView textView;
    private WifiManager wifiManager;

    @Override
    protected Fragment initFragment() {
        return null;
    }

    @Override
    protected View initContentView() {
        View view = getLayoutInflater().inflate(R.layout.activity_main_network, null);
        return view;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main_network);

        textView = (TextView) findViewById(R.id.tvStatus);
        btnCheckNetwork = (Button) findViewById(R.id.btnCheckNetwork);
        btnCheckNetwork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkNetwork();
            }
        });
    }

    void checkNetwork()
    {
    /*    SupplicantState supState;
        wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        textView.setText("Enable: " + wifiManager.isWifiEnabled());*/


//        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
//        supState = wifiInfo.getSupplicantState();
//        Log.d("dzumi", supState.name() + "/" + supState.toString());
//        textView.setText(supState.name());

        ConnectivityManager connMgr =
                (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        Log.d("dzumi", "isActiveNetworkMetered: " + connMgr.isActiveNetworkMetered());
//        Log.d("dzumi", "isDefaultNetworkActive: " + connMgr.isDefaultNetworkActive());

        NetworkInfo[] networkInfos = connMgr.getAllNetworkInfo();
        for(NetworkInfo info : networkInfos)
        {
            if(info.getType() ==  ConnectivityManager.TYPE_MOBILE || info.getType() ==  ConnectivityManager.TYPE_WIFI) {
                Log.d("dzumi", "getTypeName: " + info.getTypeName());
                Log.d("dzumi", "getReason: " + info.getReason());
                Log.d("dzumi", "getDetailedState: " + info.getDetailedState());
                Log.d("dzumi", "isAvailable: " + info.isAvailable());
                Log.d("dzumi", "isConnected: " + info.isConnected());
                Log.d("dzumi", "isConnectedOrConnecting: " + info.isConnectedOrConnecting());
                Log.d("dzumi", "isFailover: " + info.isFailover());
            }
        }

        if(networkInfo == null)
            textView.setText("Disable Network");
        else
        {
            Log.d("dzumi", "! null");
            textView.setText("TYPE: " + networkInfo.getTypeName());
            if(isOnline())
            {
                textView.setText(textView.getText() + " Connected");
            }
            else
                textView.setText(textView.getText() + " not Connected");

        }




    }
    public boolean isOnline() {

        Runtime runtime = Runtime.getRuntime();
        try {

            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int     exitValue = ipProcess.waitFor();
            return (exitValue == 0);

        } catch (IOException e)          { e.printStackTrace(); }
        catch (InterruptedException e) { e.printStackTrace(); }

        return false;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_network, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
