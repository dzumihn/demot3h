package app.dzumi.demot3h.demo.adapter.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import app.dzumi.demot3h.R;

/**
 * Created by dzumi on 16/05/2015.
 * Dung de hien thi danh sach ten quoc gia --> textView
 */
public class SampleAdapter1 extends BaseAdapter{
    Context mContext;
    int mLayoutID;
    String[] mCountries;
    public SampleAdapter1(Context context, int layoutID, String[] countries)
    {
        mContext = context;
        mLayoutID = layoutID;
        mCountries = countries;
    }
    @Override
    public int getCount() {
        //chung ta phai tra ve so luong item can duoc tao ra tren listview
        return mCountries.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;

        //buoc 1: tu layoutID(R.layout.name) --> view
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);

        view = inflater.inflate(mLayoutID, null);

        //buoc 2: find view member trong view
        TextView tvName = (TextView) view.findViewById(R.id.tvName);

        //buoc 3: set info
        tvName.setText(mCountries[position]);
        return view;
    }
}
