package app.dzumi.demot3h.demo.sqlite.contentprovider;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import app.dzumi.demot3h.R;
import app.dzumi.demot3h.demo.sqlite.ActivitySampleContentProvider;
import app.dzumi.demot3h.demo.sqlite.contentprovider.base.Country;
import app.dzumi.demot3h.demo.sqlite.contentprovider.provider.IProvider;

/**
 * Created by dzumi on 14/06/2015.
 */
public class DialogFragmentInsertCountry extends DialogFragment implements DialogInterface.OnClickListener {

    EditText edtNameEn;
    EditText edtNameVi;
    IProvider iProvider;
    ICallbackActivity iCallbackActivity;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.dialog_fragment_insert_country, null);

        edtNameEn = (EditText) view.findViewById(R.id.edtNameEn);
        edtNameVi = (EditText) view.findViewById(R.id.edtNameVi);

        //** tai sao co dong nay?
        iProvider = ((ActivitySampleContentProvider) getActivity()).mProvider;

        iCallbackActivity =  (ActivitySampleContentProvider) getActivity();
        AlertDialog alertDialog =
                new AlertDialog.Builder(getActivity()).
                        setTitle("insert Country")
                        .setPositiveButton("Insert", this)//this la gi
                        .setNegativeButton("Cancel", this)
                        .setView(view)
                        .create();
        return alertDialog;
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == DialogInterface.BUTTON_POSITIVE) {
            String nameEn = edtNameEn.getText().toString();
            String nameVi = edtNameVi.getText().toString();
            Country country = new Country();
            country.setNameEn(nameEn);
            country.setNameVi(nameVi);
            if(iProvider.insert(country) > 0)
            {
                iCallbackActivity.insertComplete(true);
            }
            else
                iCallbackActivity.insertComplete(false);
            dismiss();
        } else if (which == DialogInterface.BUTTON_NEGATIVE) {
            dialog.dismiss();
        }
    }
}
