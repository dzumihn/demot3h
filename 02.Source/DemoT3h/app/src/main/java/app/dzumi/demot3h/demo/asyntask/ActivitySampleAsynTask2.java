package app.dzumi.demot3h.demo.asyntask;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.Random;

import app.dzumi.demot3h.BaseActivity;
import app.dzumi.demot3h.R;
import app.dzumi.demot3h.main.utils.Log;

/**
 * Created by dzumi on 29/06/2015.
 */
public class ActivitySampleAsynTask2 extends BaseActivity implements View.OnClickListener {

    Button btnDownload, btnCancel;
    DownloadFilesTask myDownloadFilesTask;
    @Override
    protected Fragment initFragment() {
        return null;
    }

    @Override
    protected View initContentView() {
        return getView(R.layout.activity_thread_sample_asyntask2);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnDownload = (Button) findViewById(R.id.btnDownload);

        btnCancel.setOnClickListener(this);
        btnDownload.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if(id == R.id.btnDownload)
        {
            myDownloadFilesTask = new DownloadFilesTask();
            myDownloadFilesTask.execute("File 1", "file 2", "file 3", "file 4");
        }
        else if(id == R.id.btnCancel)
        {

        }
    }

    private class DownloadFilesTask extends AsyncTask<String, Integer, Long> {
        long downloadFile(String url)
        {
            Random random = new Random();
            try {
                Thread.sleep(random.nextInt(2000) + 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return random.nextInt(1000000);
        }


        protected Long doInBackground(String... urls) {
            int count = urls.length;
            long totalSize = 0;
            for (int i = 0; i < count; i++) {
                totalSize += downloadFile(urls[i]);
                publishProgress((int) ((i / (float) count) * 100));
                // Escape early if cancel() is called
                if (isCancelled()) break;
            }
            return totalSize;
        }

        protected void onProgressUpdate(Integer... progress) {
            Log.d(progress[0] + "");
        }

        protected void onPostExecute(Long result) {
            Toast.makeText(ActivitySampleAsynTask2.this,"Downloaded " + result + " bytes", Toast.LENGTH_SHORT).show();
        }
    }

}
