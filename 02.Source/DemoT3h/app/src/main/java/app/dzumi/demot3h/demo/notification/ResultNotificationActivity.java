package app.dzumi.demot3h.demo.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import app.dzumi.demot3h.R;

public class ResultNotificationActivity extends ActionBarActivity {

    WebView webView;
    Button btnShowNotification;
    TextView textView;
    Spinner spinner;

    String url;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_notification);

        webView = (WebView) findViewById(R.id.webView);
        btnShowNotification = (Button) findViewById(R.id.btnShowNotification);
        textView = (TextView) findViewById(R.id.tvUrl);
        spinner = (Spinner) findViewById(R.id.cmbUrl);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, MainNotificationActivity.listNumbers);
        spinner.setAdapter(adapter);

        context = this;

        btnShowNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(context, "onclick", Toast.LENGTH_SHORT).show();
                //xac dinh activity duoc open khi click vao notification item
                Intent resultIntent = new Intent(context, ResultNotificationActivity.class);

                int pos = Integer.parseInt(spinner.getSelectedItem().toString()) - 1;

                resultIntent.putExtra(MainNotificationActivity.KEY_URL,
                        MainNotificationActivity.listUrls[pos]);

                TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                // Adds the back stack
                stackBuilder.addParentStack(ResultNotificationActivity.class);
                // Adds the Intent to the top of the stack
                stackBuilder.addNextIntent(resultIntent);
                // Gets a PendingIntent containing the entire back stack

                PendingIntent resultPendingIntent =
                        stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

                NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                        .setContentTitle("Test")
                        .setContentText(MainNotificationActivity.listUrls[pos])
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setAutoCancel(true);
                builder.setContentIntent(resultPendingIntent);

                NotificationManager mNotificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                mNotificationManager.notify(1, builder.build());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        url = intent.getStringExtra(MainNotificationActivity.KEY_URL);
        textView.setText(url);
        webView.loadUrl(url);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_result_notification, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
