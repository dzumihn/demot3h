package app.dzumi.demot3h.demo.storage;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import app.dzumi.demot3h.R;
import butterknife.Bind;
import butterknife.ButterKnife;

public class ActivitySampleSharedPreference extends ActionBarActivity {
    @Bind(R.id.edtName) EditText edtName;
    @Bind(R.id.ckbNam)CheckBox ckbNam;
    @Bind(R.id.btnSave) Button btnSave;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_sample_shared_preference);

        //
        ButterKnife.bind(this);
        initData();
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //khi bam save --> luu thong tin name & check vao sharedpreference
                SharedPreferences.Editor editor = getSharedPreferences("sharedPref", MODE_PRIVATE).edit();
                editor.putString("name", edtName.getText().toString());
                editor.putBoolean("nam", ckbNam.isChecked());

                editor.commit();
            }
        });
    }

    void initData()
    {
        //khi mo activity, load thong tin tu sharedPReference vao cac view tuong ung (neu co)
        SharedPreferences sharedPreferences = getSharedPreferences("sharedPref", MODE_PRIVATE);
        String name = sharedPreferences.getString("name", "no name");
        boolean isNam = sharedPreferences.getBoolean("nam", false);

        edtName.setText(name);
        ckbNam.setChecked(isNam);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_sample_shared_preference, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
