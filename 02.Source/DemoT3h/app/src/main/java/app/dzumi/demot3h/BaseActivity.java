package app.dzumi.demot3h;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;

import app.dzumi.demot3h.main.Constants;
import app.dzumi.demot3h.main.fragment.NavDrawerFragment;
import app.dzumi.demot3h.main.utils.Log;
import app.dzumi.demot3h.main.utils.logger.LogWrapper;
import lib.dzumi.nav.util.MaterialDrawerLayout;

/**
 * Created by dzumi on 07/03/2015.
 */
public abstract class BaseActivity extends ActionBarActivity {
    //layout properties
    //nav & toolbar
    protected NavDrawerFragment mNavDrawer;
    protected MaterialDrawerLayout mLayoutDrawer;
    protected Toolbar mToolbar;
    protected FrameLayout mFrameContainer;

    //
    FragmentManager mFragmentManager;

     /**
     * Khoi tao fragment cho activity, fragment nay se dc gan vao frameContainer
     * @return
     */
    protected abstract Fragment initFragment();

    protected abstract View initContentView();
    /**
     * Khoi tao icon cho button navigation tren toolbar
     * @return
     */
    protected int initNavigationIconToolbar()
    {
        return R.drawable.ic_arrow_back;
    }

    protected String initTitleToolbar()
    {
        String path = getIntent().getStringExtra(Constants.TITLE_ACTIVITY);
        if(path == null)
            return "";
        return path;
    }
    /**
     * Show life cycle log
     * @return true - show log
     */
    protected boolean isShowLifeCycleLog()
    {
        return false;
    }

    protected boolean isInitializeLogging()
    {
        return false;
    }

    protected boolean justCallSuper()
    {
        return false;
    }
    /**
     * Event when click Navigation Icon in toolbar
     */
    protected void onNavigationOnClickListener()
    {
        onBackPressed();
    }

    /**
     * open navigation drawer
     * @param gravity
     */
    public void openNavigationDrawer(int gravity)
    {
        mLayoutDrawer.openDrawer(gravity);
    }

    /**
     * close navigation drawer
     */
    public void closeNavigationDrawer()
    {
        mLayoutDrawer.closeDrawers();
    }


    protected View getView(int idLayout){
       return getLayoutInflater().inflate(idLayout, null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(!justCallSuper()) {
            setContentView(R.layout.activity_main);

            if (isShowLifeCycleLog())
                Log.d("onCreate - " + getClass().toString());


            mFragmentManager = getFragmentManager();

            Fragment currentFragment = initFragment();
            if (currentFragment != null)
                mFragmentManager.beginTransaction().replace(R.id.frame_container, initFragment()).commit();
            else {
                View view = initContentView();
                mFrameContainer = (FrameLayout) findViewById(R.id.frame_container);
                if (view == null) {
                    throw new RuntimeException("You must init fragment or contentView.");
                }

                mFrameContainer.addView(view);
            }
            initView();
        }
    }

    void initView()
    {
//        mNavDrawer = (NavDrawerFragment) mFragmentManager.findFragmentById(R.id.navigation_drawer);
        mLayoutDrawer = (MaterialDrawerLayout) findViewById(R.id.drawer_layout);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(initNavigationIconToolbar());
        mToolbar.setBackgroundColor(getResources().getColor(R.color.primary));
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        mToolbar.setTitle(initTitleToolbar());
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onNavigationOnClickListener();
            }
        });
//


    }
    protected void hideToolbar()
    {
        mToolbar.setVisibility(View.GONE);
    }

    protected void showToolbar()
    {
        mToolbar.setVisibility(View.VISIBLE);
    }

    //region Life cycle
    @Override
    protected void onStart() {
        super.onStart();
        if(isInitializeLogging())
         initializeLogging();
        if(isShowLifeCycleLog())
            Log.d("OnStart - " + getClass().toString());
    }



    /** Set up targets to receive log data */
    public void initializeLogging() {
        // Using Log, front-end to the logging chain, emulates android.util.log method signatures.
        // Wraps Android's native log framework
        LogWrapper logWrapper = new LogWrapper();
        app.dzumi.demot3h.main.utils.logger.Log.setLogNode(logWrapper);

        app.dzumi.demot3h.main.utils.logger.Log.i("dzumi", "Ready");
    }
    @Override
    protected void onResume() {
        super.onResume();
        if(isShowLifeCycleLog())
            Log.d("onResume - " + getClass().toString());
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(isShowLifeCycleLog())
            Log.d("onPause - " + getClass().toString());
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(isShowLifeCycleLog())
            Log.d("onStop - " + getClass().toString());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(isShowLifeCycleLog())
            Log.d("onDestroy - " + getClass().toString());
    }
    //endregion
}
