package app.dzumi.demot3h.demo.internet.json;

import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import app.dzumi.demot3h.main.utils.Log;

/**
 * Created by dzumi on 24/07/2015.
 */
public class GetAPIKeyBasic extends AsyncTask<String, Void, String>{

    @Override
    protected String doInBackground(String... params) {
        try {
            URL url  = new URL(params[0]);
            URLConnection urlConnection = url.openConnection();
            HttpURLConnection httpURLConnection = (HttpURLConnection) urlConnection;
            if(httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK)
            {
                //doc ghi file
                InputStream inputStream = httpURLConnection.getInputStream();
                byte[] buffers = new byte[1000];
                int i = 0;
                String s = "";
                while ((i = inputStream.read(buffers)) != -1)
                {
                    s+= new String(buffers, 0, i);
                }
                Log.d(s);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

    }
}
