package app.dzumi.demot3h.demo.sqlite;

import android.app.Fragment;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import app.dzumi.demot3h.BaseActivity;
import app.dzumi.demot3h.R;
import app.dzumi.demot3h.demo.sqlite.contentprovider.DialogFragmentInsertCountry;
import app.dzumi.demot3h.demo.sqlite.contentprovider.ICallbackActivity;
import app.dzumi.demot3h.demo.sqlite.contentprovider.adapter.CountryAdapter;
import app.dzumi.demot3h.demo.sqlite.contentprovider.base.Country;
import app.dzumi.demot3h.demo.sqlite.contentprovider.provider.IProvider;
import app.dzumi.demot3h.demo.sqlite.contentprovider.provider.Provider3;

/**
 * Created by dzumi on 14/06/2015.
 */
public class ActivitySampleContentProvider extends BaseActivity implements ICallbackActivity{
    @Override
    protected Fragment initFragment() {
        return null;
    }

    @Override
    protected View initContentView() {
        return getView(R.layout.activity_content_provider_sample);
    }

    @Override
    protected boolean isShowLifeCycleLog() {
        return true;
    }

    ListView listView;
    CountryAdapter mAdapter;
    List<Country> mCountryList;
    public IProvider mProvider;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listView = (ListView) findViewById(R.id.listView);
        registerForContextMenu(listView);
        mProvider = new Provider3(this);
        initData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_list_country, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.mnAdd)
        {
            DialogFragmentInsertCountry dialogFragment = new DialogFragmentInsertCountry();
            dialogFragment.show(getFragmentManager(), "dialog");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.floating_menu_list_view, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int id = item.getItemId();
        //van de dat ra o day la gi??????
        //==> xac dinh duoc doi tuong dang tuong tac la?
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        if(id == R.id.mnDelete)
        {
            Toast.makeText(this, "position: " + info.position , Toast.LENGTH_SHORT).show();
            return true;
        }else if(id == R.id.mnUpdate)
        {
            Toast.makeText(this, "update", Toast.LENGTH_SHORT).show();
            return true;
        }
        return super.onContextItemSelected(item);
    }

    void initData()
    {
        mCountryList = mProvider.get();
        mAdapter = new CountryAdapter(this, R.layout.item_list_view_countries1, mCountryList);
        listView.setAdapter(mAdapter);
    }

    @Override
    public void insertComplete(boolean isSuccess) {
        if(isSuccess)
        {
            Toast.makeText(this, "insert success", Toast.LENGTH_SHORT).show();
            initData();
        }
        else
        {
            Toast.makeText(this, "insert fail", Toast.LENGTH_SHORT).show();
        }
    }
}
