package app.dzumi.demot3h.demo.adapter;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import app.dzumi.demot3h.BaseActivity;
import app.dzumi.demot3h.R;
import app.dzumi.demot3h.demo.adapter.adapter.CountryAdapter;
import app.dzumi.demot3h.demo.adapter.model.Country;

/**
 * Created by dzumi on 15/05/2015.
 */
public class SampleListCountriesActivity extends BaseActivity {
    ListView listView;
    List<Country> mCountries;

    @Override
    protected Fragment initFragment() {
        return null;
    }

    @Override
    protected View initContentView() {
        return getView(R.layout.layout_listview);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData();

        listView = (ListView) findViewById(R.id.listview);

        //tao adapter
       /* ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.item_list_view_sample1, mCountries );*/
       /*  ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, mCountries );*/

        CountryAdapter adapter = new CountryAdapter(this,
                R.layout.item_list_view_countries1, mCountries);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(SampleListCountriesActivity.this, mCountries.get(position).getNameEn(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(SampleListCountriesActivity.this, CountryDetailActivity.class);

                //truyen du lieu
                Bundle bundle = new Bundle();
                bundle.putString("nameEn", mCountries.get(position).getNameEn());
                bundle.putString("capital", mCountries.get(position).getCapital());
                intent.putExtra("item", bundle);

                startActivity(intent);
            }
        });
    }

    void initData()
    {
        String[] arrayNameVi =getResources().getStringArray(R.array.countries_vi);
        String[] arrayNameEn =getResources().getStringArray(R.array.countries_en);
//        TypedArray arrayFlag = getResources().obtainTypedArray(R.array.flags);
        int[] flags = {R.drawable.cambodia,R.drawable.canada,
                R.drawable.united_states,R.drawable.argentina,
                R.drawable.poland,R.drawable.turkey,
                R.drawable.vietnam,R.drawable.italy,
                R.drawable.england,R.drawable.spain,
                R.drawable.china,R.drawable.thailand,
                R.drawable.germany,R.drawable.korea,
                R.drawable.japan};

        mCountries = new ArrayList<>();
        for(int i = 0; i < arrayNameEn.length; i++)
        {
            Country country = new Country(arrayNameEn[i], arrayNameVi[i],
                    flags[i]);
            mCountries.add(country);
        }
    }
    
    
}
