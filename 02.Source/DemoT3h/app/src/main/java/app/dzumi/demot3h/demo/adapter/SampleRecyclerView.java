package app.dzumi.demot3h.demo.adapter;

import android.app.Fragment;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import app.dzumi.demot3h.BaseActivity;
import app.dzumi.demot3h.R;

/**
 * Created by dzumi on 17/05/2015.
 */
public class SampleRecyclerView extends BaseActivity {
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private SampleRecyclerAdapter mAdapter;
    private String[] mData;

    @Override
    protected Fragment initFragment() {
        return null;
    }

    @Override
    protected View initContentView() {
        return getView(R.layout.activity_adapter_recyclerview);
    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mData = getResources().getStringArray(R.array.countries_vi);
        // specify an adapter (see also next example)
        mAdapter = new SampleRecyclerAdapter(mData);
        mRecyclerView.setAdapter(mAdapter);
    }
}
