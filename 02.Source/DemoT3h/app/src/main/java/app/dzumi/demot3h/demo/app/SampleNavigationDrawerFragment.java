package app.dzumi.demot3h.demo.app;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import app.dzumi.demot3h.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class SampleNavigationDrawerFragment extends Fragment {

    ImageView imageView;
    int resource;
    public SampleNavigationDrawerFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sample_navigation_drawer, container, false);
        imageView = (ImageView) view.findViewById(R.id.ivFlag);
        imageView.setImageResource(resource);
        return view;
    }

    //activity goi khi thay doi resource
    public void setResource(int resource)
    {
        imageView.setImageResource(resource);

    }
}
