package app.dzumi.demot3h.demo.adapter.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import app.dzumi.demot3h.R;
import app.dzumi.demot3h.demo.adapter.model.Country;

/**
 * Created by dzumi on 16/05/2015.
 */
public class CountryAdapter2 extends ArrayAdapter<Country> {
    Context mContext;
    int mLayoutID;
    List<Country> mCountries;

    public CountryAdapter2(Context context, int layoutID, List<Country> countries) {
        super(context, layoutID, countries);
        mContext = context;
        mLayoutID = layoutID;
        mCountries = countries;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        //lan dau
        if (convertView == null) {
            //buoc 1: tu layoutID(R.layout.name) --> view
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(mLayoutID, null);

            //buoc 2: find view member trong view
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else //lan tiep theo
            viewHolder = (ViewHolder) convertView.getTag();

        //buoc 3: set info
        viewHolder.tvNameVi.setText(mCountries.get(position).getNameVi());
        viewHolder.tvNameEn.setText(mCountries.get(position).getNameEn());
        viewHolder.ivFlag.setImageResource(mCountries.get(position).getFlagID());

        return convertView;
    }

    class ViewHolder {
        //lop quan ly cac member view trong convertview
        TextView tvNameVi;
        TextView tvNameEn;
        ImageView ivFlag;

        ViewHolder(View rootView) {
            tvNameVi = (TextView) rootView.findViewById(R.id.tvNameVi);
            tvNameEn = (TextView) rootView.findViewById(R.id.tvNameEn);
            ivFlag = (ImageView) rootView.findViewById(R.id.ivFlag);
        }
    }
}
