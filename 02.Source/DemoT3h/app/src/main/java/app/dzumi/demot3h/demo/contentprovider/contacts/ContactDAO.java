package app.dzumi.demot3h.demo.contentprovider.contacts;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.SipAddress;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by dzumi on 06/07/2015.
 */
public class ContactDAO {
    Context mContext;
    public ContactDAO(Context context)
    {
        mContext = context;
    }

    public List<MyContact> get()
    {
        //muon truy xuat content provider thong qua contentResolver
        ContentResolver contentResolver = mContext.getContentResolver();
        Cursor cursor = contentResolver.query(ContactsContract.Data.CONTENT_URI, null, null, null, ContactsContract.Data.RAW_CONTACT_ID + " ASC");
        return fetchAll(cursor);
    }

    private List<MyContact> fetchAll(Cursor cursor)
    {
        List<MyContact> contacts = new ArrayList<>();
        long rawID = 0;
        if(cursor != null) {
            MyContact myContact = null;
            if (cursor.moveToFirst()) {
                do {
                    long id = cursor.getLong(
                            cursor.getColumnIndex(ContactsContract.Data.RAW_CONTACT_ID));
                    if(rawID != id)
                    {
                        myContact = new MyContact();
                        contacts.add(myContact);
                        rawID = id;
                    }
                    String mimeType = cursor.getString(
                            cursor.getColumnIndex(ContactsContract.Data.MIMETYPE));
                    //CONTENT_ITEM_TYPE == MIME_TYPE
                    switch (mimeType)
                    {
                        //ten
                        case StructuredName.CONTENT_ITEM_TYPE:
                            //display name --> DATA1
                            myContact.setName(cursor.getString(
                                    cursor.getColumnIndex(StructuredName.DISPLAY_NAME)
                            ));
                            break;
                        case Phone.CONTENT_ITEM_TYPE: //phone
                            //muon lay sdt nha
                            //kiem tra so dt co phai la so dien thoai nha hay ko
                            //dua vao TYPE --> DATA 2
                            int typePhone = cursor.getInt(cursor.getColumnIndex(Phone.TYPE));
                            String phoneNumber = cursor.getString(cursor.getColumnIndex(Phone.NUMBER));
                            if(typePhone == Phone.TYPE_HOME)
                                myContact.setPhone(phoneNumber);
                            else
                            {
                                //ko set
                            }
                            break;
                        case Email.CONTENT_ITEM_TYPE: //email
                            break;
                        case SipAddress.CONTENT_ITEM_TYPE: //address
                            break;
                    }
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return contacts;
    }


}
