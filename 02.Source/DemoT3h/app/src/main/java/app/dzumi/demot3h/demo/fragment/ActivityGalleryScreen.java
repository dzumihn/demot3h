package app.dzumi.demot3h.demo.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.view.ViewPager;
import android.view.View;

import app.dzumi.demot3h.BaseActivity;
import app.dzumi.demot3h.R;

/**
 * Created by dzumi on 25/05/2015.
 */
public class ActivityGalleryScreen extends BaseActivity {
    @Override
    protected Fragment initFragment() {
        return null;
    }

    @Override
    protected View initContentView() {
        return getView(R.layout.activity_gallery_screen);
    }

    ViewPager viewPager;
    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(new GalleryAdapter(getFragmentManager(),
                getIntent().getStringArrayExtra("listImage"),
                getIntent().getStringExtra("parent")));
        viewPager.setCurrentItem(getIntent().getIntExtra("pos", 0));
    }
}
