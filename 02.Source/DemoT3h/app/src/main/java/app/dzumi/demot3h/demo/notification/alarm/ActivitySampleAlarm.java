package app.dzumi.demot3h.demo.notification.alarm;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import app.dzumi.demot3h.R;

public class ActivitySampleAlarm extends Activity implements OnClickListener {

	private PendingIntent mAlarmIntent;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sample_alarm);
		findViewById(R.id.start).setOnClickListener(this);
		findViewById(R.id.stop).setOnClickListener(this);
		Intent launchIntent = new Intent(this, AlarmReceiver.class);

		mAlarmIntent = PendingIntent.getBroadcast(this, 0, launchIntent, 0);
	}

	@Override
	public void onClick(View v) {
		AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		long interval = 5 * 1000; // 5 gi�y.

		switch (v.getId()) {
		case R.id.start:
			Toast.makeText(this, "Scheduled", Toast.LENGTH_SHORT).show();
			manager.setRepeating(AlarmManager.ELAPSED_REALTIME,
					SystemClock.elapsedRealtime() + interval, interval,
					mAlarmIntent);
			break;
		case R.id.stop:
			Toast.makeText(this, "Canceled", Toast.LENGTH_SHORT).show();
			manager.cancel(mAlarmIntent);
			break;
		default:
			break;
		}
	}
}
