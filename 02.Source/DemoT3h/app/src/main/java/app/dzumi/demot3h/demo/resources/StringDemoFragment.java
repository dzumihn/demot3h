package app.dzumi.demot3h.demo.resources;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import app.dzumi.demot3h.R;
import app.dzumi.demot3h.main.fragment.BaseFragment;

/**
 * Created by dzumi on 01/05/2015.
 */
public class StringDemoFragment extends BaseFragment {

    static final String TAG = "StringDemoFragment";
    TextView tvSingleQuotation, tvDoubleQuotation, tvHtmlTag, tvStringFormat, tvStringFormat2, tvStringPlural, tvStringPlural2;
    @Override
    protected int layoutID() {
        return R.layout.fragment_string_demo;
    }

    @Override
    protected void initViews(View root, Bundle savedInstanceState) {
        tvDoubleQuotation = findView(R.id.tvDoubleQuotation);
        tvSingleQuotation = findView(R.id.tvSingleQuotation);
        tvHtmlTag = findView(R.id.tvHtmlTag);
        tvStringFormat = findView(R.id.tvStringFormat);
        tvStringFormat2 = findView(R.id.tvStringFormat2);
        tvStringPlural = findView(R.id.tvStringPlural);
        tvStringPlural2 = findView(R.id.tvStringPlural2);
    }

    @Override
    protected void setupViews() {
        //cach 1: default: JAVA CPDE muon try cap tat ca cac tai nguyen deu thong qua Resource
        Resources resources = getResources();
        String double_quotation_mark = resources.getString(R.string.double_quotation_mark);
        tvDoubleQuotation.setText(double_quotation_mark);

        //cach 2: oi vi STring la mot doi tuong dac biet --> khong can thong qua Resource
        String html_tag = getString(R.string.html_tag);
        tvHtmlTag.setText(html_tag);

        //cach 3: boi vi STring la mot doi tuong rat dac biet --> khong can get String khi settext cho view
        tvSingleQuotation.setText(R.string.single_quotation_mark);

        //string format
        String format = getString(R.string.string_format, 5);
        tvStringFormat.setText(format);

        //string format2
        String format2 = getString(R.string.string_format_2, "Midu", 5);
        tvStringFormat2.setText(format2);

        //string plural
        String plural = resources.getQuantityString(R.plurals.numberOfSongsAvailable, 1);
        tvStringPlural.setText(plural);

        String plural2 = resources.getQuantityString(R.plurals.numberOfSongsAvailable,2, 5 );
        tvStringPlural2.setText(plural2);
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }
}
