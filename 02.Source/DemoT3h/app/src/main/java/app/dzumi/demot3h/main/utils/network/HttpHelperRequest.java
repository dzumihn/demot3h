package app.dzumi.demot3h.main.utils.network;

import android.content.Context;

import org.json.JSONObject;



public abstract class HttpHelperRequest implements IHttpExecute {
    protected abstract void onResponseListener(JSONObject jsonResponse);
    public abstract String getTAG();
    public void cancel(Context context) {
        HttpHelper.getInstance(context).cancelRequest(getTAG());
    }




}
