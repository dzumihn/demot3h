package app.dzumi.demot3h.demo.sqlite.contentprovider.base;

public class Migration_0001 extends Migration {
    public Migration_0001() {
        steps.add("CREATE TABLE " + DBContract.Country.TABLE_NAME
                + " (" + DBContract.Country._ID + " INTEGER PRIMARY KEY, "
                + DBContract.Country.NAME_EN + " TEXT, "
                + DBContract.Country.NAME_VI + " TEXT, "
                + DBContract.Country.FLAG + " TEXT, "
                + DBContract.Country.POPULATION + " REAL, "
                + DBContract.Country.CAPTION + " TEXT)");

    }
}