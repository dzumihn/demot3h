package app.dzumi.demot3h.demo.sqlite.dao;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import app.dzumi.demot3h.demo.sqlite.model.Country;

/**
 * Created by dzumi on 01/06/2015.
 */
public class CountryDAO2 extends BaseDAO{
    public CountryDAO2(Context context)
    {
        super.setContext(context);
    }

    public List<Country> get(){
        Cursor cursor = loadAll(Country.TABLE_NAME, null);
        return fetchAll(cursor);
    }

    List<Country> fetchAll(Cursor cursor)
    {
        List<Country> countries  = new ArrayList<>();
        if(cursor.moveToFirst())
        {
            do{
                countries.add(fetch(cursor));
            }while (cursor.moveToNext());
        }
        cursor.close();
        return countries;
    }

    Country fetch(Cursor cursor)
    {
        Country country = new Country();
        country.set_id(cursor.getInt(cursor.getColumnIndex(Country._ID)));
        country.setNameEn(cursor.getString(cursor.getColumnIndex(Country.NAME_EN)));

        //chuyen tu ten --> id
        Resources resources = context.getResources();
        country.setFlagID(resources.getIdentifier(country.getFlag().split(".")[0], "drawable", context.getPackageName()));
//        country.setFlagID(resources.getIdentifier(country.getFlag().substring(0, country.getFlag().length() - 4), "drawable", context.getPackageName()));

        return country;
    }

}
