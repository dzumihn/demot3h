package app.dzumi.demot3h.demo.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import app.dzumi.demot3h.main.utils.Log;

/**
 * Created by dzumi on 13/07/2015.
 */
public class BaseServices extends Service {
    final String TAG = "sampleServices";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("onCreate - " + this.getClass().toString());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("onStartCommand" + this.getClass().toString());
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("onDestroy" + this.getClass().toString());
    }
}
