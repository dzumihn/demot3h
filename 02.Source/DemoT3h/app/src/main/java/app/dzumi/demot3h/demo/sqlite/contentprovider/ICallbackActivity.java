package app.dzumi.demot3h.demo.sqlite.contentprovider;

/**
 * Created by dzumi on 14/06/2015.
 */
public interface ICallbackActivity {
    void insertComplete(boolean isSuccess);
}
