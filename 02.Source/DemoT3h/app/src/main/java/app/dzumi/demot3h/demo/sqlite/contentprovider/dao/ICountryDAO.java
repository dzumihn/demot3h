package app.dzumi.demot3h.demo.sqlite.contentprovider.dao;

import java.util.List;

import app.dzumi.demot3h.demo.sqlite.contentprovider.base.Country;


/**
 * Created by dzumi on 14/06/2015.
 */
public interface ICountryDAO {
    List<Country> get();
    Country get(int id);

    long insert(Country country);
    int delete(int id);
    int update(int id, String nameEn);
}
