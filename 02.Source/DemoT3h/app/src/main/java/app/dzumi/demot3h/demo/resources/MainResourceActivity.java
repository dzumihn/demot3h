/*
* Copyright 2013 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package app.dzumi.demot3h.demo.resources;

import android.app.Fragment;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import app.dzumi.demot3h.MainPagerActivity;
import app.dzumi.demot3h.R;
import app.dzumi.demot3h.main.fragment.SlidingTabsFragment;
import app.dzumi.demot3h.main.fragment.SlidingTabsFragment.PagerItem;

/**
 * A simple launcher activity containing a summary sample description, sample log and a custom
 * {@link android.support.v4.app.Fragment} which can display a view.
 * <p>
 * For devices with displays with a width of 720dp or greater, the sample log is always visible,
 * on other devices it's visibility is controlled by an item on the Action Bar.
 */
public class MainResourceActivity extends MainPagerActivity {

    public static final String TAG = "MainActivity";

    // Whether the Log Fragment is currently shown
    private boolean mLogShown;


    TextView tvLog;


    @Override
    protected Fragment initFragment() {
        mTabs = new ArrayList<>();
        initData();
        return new SlidingTabsFragment();
    }

    @Override
    protected View initContentView() {
        return super.initContentView();
    }


    void initData() {

        //ten file
        //muc tieu: ten file --> id
        int id = getResources().getIdentifier("", "drawable", getPackageName());

        mTabs.add(new PagerItem(
                getString(R.string.title_string_demo), // Title
                Color.BLUE, // Indicator color
                Color.GRAY // Divider color
        ));
        mTabs.get(0).setFragment(new StringDemoFragment());

        mTabs.add(new PagerItem(
                getString(R.string.title_color_state_demo), // Title
                Color.BLUE, // Indicator color
                Color.GRAY // Divider color
        ));
        mTabs.get(1).setFragment(new ColorStateListDemoFragment());
    }
 /*   @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem logToggle = menu.findItem(R.id.menu_toggle_log);
        logToggle.setVisible(findViewById(R.id.sample_output) instanceof ViewAnimator);
        logToggle.setTitle(mLogShown ? R.string.title_menu_hide_log : R.string.title_menu_show_log);

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.menu_toggle_log:
                mLogShown = !mLogShown;
                ViewAnimator output = (ViewAnimator) findViewById(R.id.sample_output);
                if (mLogShown) {
                    output.setDisplayedChild(1);
                } else {
                    output.setDisplayedChild(0);
                }
                supportInvalidateOptionsMenu();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }*/

 /*   *//** Create a chain of targets that will receive log data *//*
    @Override
    public void initializeLogging() {
        // Wraps Android's native log framework.
        LogWrapper logWrapper = new LogWrapper();
        // Using Log, front-end to the logging chain, emulates android.util.log method signatures.
        Log.setLogNode(logWrapper);

        // Filter strips out everything except the message text.
        MessageOnlyLogFilter msgFilter = new MessageOnlyLogFilter();
        logWrapper.setNext(msgFilter);

        // On screen logging via a fragment with a TextView.
        LogFragment logFragment = (LogFragment) getSupportFragmentManager()
                .findFragmentById(R.id.log_fragment);
        msgFilter.setNext(logFragment.getLogView());

        Log.i(TAG, "Ready");
    }*/

}
