package app.dzumi.demot3h.demo.intent;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import app.dzumi.demot3h.BaseActivity;
import app.dzumi.demot3h.R;

/**
 * Created by dzumi on 29/05/2015.
 */
public class ActivityIntentSampleReturnResult extends BaseActivity
        implements View.OnClickListener {
    public final static String EXTRA_X = "extra_x";
    public final static String EXTRA_Y = "extra_y";
    public final static String EXTRA_KQ = "extra_kq";
    public final static String EXTRA_ERROR = "extra_error";
    @Override
    protected Fragment initFragment() {
        return null;
    }

    @Override
    protected View initContentView() {
        return getView(R.layout.activity_intent_sample_return_result);
    }

    Button btnCong, btnTru, btnNhan, btnChia;
    double x, y;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        btnCong = (Button) findViewById(R.id.btnCong);
        btnTru = (Button) findViewById(R.id.btnTru);
        btnNhan = (Button) findViewById(R.id.btnNhan);
        btnChia = (Button) findViewById(R.id.btnChia);

        btnCong.setOnClickListener(this);
        btnTru.setOnClickListener(this);
        btnNhan.setOnClickListener(this);
        btnChia.setOnClickListener(this);

        //init data
        Bundle bundle = getIntent().getExtras();
        x = bundle.getDouble(EXTRA_X);
        y = bundle.getDouble(EXTRA_Y);
    }

    public final static int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1;
    @Override
    public void onClick(View v) {
        int id = v.getId();

        Intent intent = getIntent();//***
        int resultCode = -1;
        String error = "";
        double kq = 0;
        if (id == R.id.btnCong) {
            kq = x+ y;
            resultCode = RESULT_OK;
        }else if (id == R.id.btnTru) {
            kq = x - y;
            resultCode = RESULT_OK;
        }else if (id == R.id.btnNhan) {
            kq = x * y;
            resultCode = RESULT_OK;
        }else if (id == R.id.btnChia) {
            if(y == 0)
            {
                error = "Syntax error";
                resultCode = RESULT_CANCELED;
            }
            else
            {
                kq = x / y;
                resultCode = RESULT_OK;
            }
        }

        Bundle bundle = intent.getExtras();
        bundle.putDouble(EXTRA_KQ, kq);
        bundle.putString(EXTRA_ERROR, error);
        intent.putExtras(bundle);
        setResult(resultCode, intent);
        finish();
    }

}
