package app.dzumi.demot3h.demo.adapter;

import android.app.Fragment;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import app.dzumi.demot3h.BaseActivity;
import app.dzumi.demot3h.R;
import app.dzumi.demot3h.demo.adapter.adapter.SampleAdapter1;

/**
 * Created by dzumi on 15/05/2015.
 */
public class SampleListViewActivity extends BaseActivity {
    ListView listView;
    String[] mCountries;

    @Override
    protected Fragment initFragment() {
        return null;
    }

    @Override
    protected View initContentView() {
        return getView(R.layout.layout_listview);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData();

        listView = (ListView) findViewById(R.id.listview);

        //tao adapter
       /* ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.item_list_view_sample1, mCountries );*/
       /*  ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, mCountries );*/

        SampleAdapter1 adapter = new SampleAdapter1(this, R.layout.item_list_view_sample2, mCountries);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(SampleListViewActivity.this, mCountries[position], Toast.LENGTH_SHORT).show();
            }
        });
    }

    void initData()
    {
        mCountries = getResources().getStringArray(R.array.countries_vi);
    }
}
