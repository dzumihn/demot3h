package app.dzumi.demot3h.demo.adapter;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import app.dzumi.demot3h.R;

public class CountryDetailActivity extends ActionBarActivity {
    TextView tvName, tvCapital;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_detail);
        tvCapital = (TextView) findViewById(R.id.tvCapital);
        tvName = (TextView) findViewById(R.id.tvName);

        //lay du lieu dc gui kem tu intent
        Bundle bundle = getIntent().getBundleExtra("item");
        String nameEn = bundle.getString("nameEn", "default");
        String capital = bundle.getString("capital", "default");
        tvName.setText(nameEn);
        tvCapital.setText(capital);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_country_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
