package app.dzumi.demot3h.demo.menu;

import android.app.Fragment;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.List;

import app.dzumi.demot3h.BaseActivity;
import app.dzumi.demot3h.R;
import app.dzumi.demot3h.demo.sqlite.contentprovider.ICallbackActivity;
import app.dzumi.demot3h.demo.sqlite.contentprovider.adapter.CountryAdapter;
import app.dzumi.demot3h.demo.sqlite.contentprovider.base.Country;
import app.dzumi.demot3h.demo.sqlite.contentprovider.provider.IProvider;
import app.dzumi.demot3h.demo.sqlite.contentprovider.provider.Provider3;

/**
 * Created by dzumi on 14/06/2015.
 */
public class ActivitySampleActionMode extends BaseActivity implements ICallbackActivity{
    @Override
    protected Fragment initFragment() {
        return null;
    }

    @Override
    protected View initContentView() {
        return getView(R.layout.activity_content_provider_sample);
    }

    @Override
    protected boolean isShowLifeCycleLog() {
        return true;
    }

    ListView listView;
    CountryAdapter mAdapter;
    List<Country> mCountryList;
    public IProvider mProvider;
    SearchView searchView;

    AbsListView.MultiChoiceModeListener listener = new AbsListView.MultiChoiceModeListener() {

        //su kien khi click vao 1 item tren listview
        @Override
        public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
            String title = "Selected item: " + listView.getCheckedItemCount();
            mode.setTitle(title);
        }

        //bat su kien click item menu
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

            return false;
        }

        //action mode
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            getMenuInflater().inflate(R.menu.floating_menu_list_view, menu);
            hideToolbar();
            mode.setTitle("Selected item: 0");
            return true;
        }

        //chua bi tao action mode, dc su dung de prepare data trong mot so truong hop
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        //huy action mode
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            showToolbar();
        }




    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listView = (ListView) findViewById(R.id.listView);

        //dang ki che do su actionMode cho listView
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

        //thiet lap vong doi cua actionMode qua phuong thuc
        listView.setMultiChoiceModeListener(listener);

        mProvider = new Provider3(this);
        initData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_list_country, menu);

        //tim kiem menu item chua searchView
        MenuItem menuItem = menu.findItem(R.id.mnSearch);
        ///searchView duoc khai bao la actionView trong menuItem
        //--> de co doi tuong searchView can thuc hien menuItem.getActionView & ep kieu ve searchView
        searchView = (SearchView) menuItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            //khi nhan enter tren ban phim
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            //khi go ky tu bat ky tren ban phim
            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return true;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.mnAdd)
        {
            DialogFragmentInsertCountry dialogFragment = new DialogFragmentInsertCountry();
            dialogFragment.show(getFragmentManager(), "dialog");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    void initData()
    {
        mCountryList = mProvider.get();
        mAdapter = new CountryAdapter(this, R.layout.item_list_view_countries1, mCountryList);
        listView.setAdapter(mAdapter);
    }

    @Override
    public void insertComplete(boolean isSuccess) {
        if(isSuccess)
        {
            Toast.makeText(this, "insert success", Toast.LENGTH_SHORT).show();
            initData();
        }
        else
        {
            Toast.makeText(this, "insert fail", Toast.LENGTH_SHORT).show();
        }
    }
}
