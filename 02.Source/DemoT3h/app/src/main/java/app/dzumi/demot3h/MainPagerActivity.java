/*
* Copyright 2013 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package app.dzumi.demot3h;

import android.app.Fragment;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.ViewAnimator;

import java.util.List;

import app.dzumi.demot3h.main.fragment.BaseFragment;
import app.dzumi.demot3h.main.fragment.SlidingTabsFragment;
import app.dzumi.demot3h.main.interfaces.OnPagerChanged;

/**
 * A simple launcher activity containing a summary sample description, sample log and a custom
 * {@link Fragment} which can display a view.
 * <p>
 * For devices with displays with a width of 720dp or greater, the sample log is always visible,
 * on other devices it's visibility is controlled by an item on the Action Bar.
 */
public class MainPagerActivity extends BaseActivity implements OnPagerChanged {

    protected TextView tvLog;

    public static final String TAG = "MainPagerActivity";

    // Whether the Log Fragment is currently shown
    private boolean mLogShown;
    protected List<SlidingTabsFragment.PagerItem> mTabs;

    public List<SlidingTabsFragment.PagerItem> getTabs()
    {
        return mTabs;
    }
    @Override
    public void onPagerChanged(String log) {
        tvLog.setText(log);
    }

    @Override
    protected Fragment initFragment() {
        return null;
    }

    @Override
    protected View initContentView() {
        return getView(R.layout.activity_main_tab_pager);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main_tab_pager);
        setContentView(R.layout.activity_main);

        mFragmentManager = getFragmentManager();

        View view = initContentView();
        mFrameContainer = (FrameLayout) findViewById(R.id.frame_container);

        SlidingTabsFragment currentFragment;
        mFrameContainer.addView(view);
//        if (savedInstanceState == null) {
            currentFragment = (SlidingTabsFragment) initFragment();
            mFragmentManager.beginTransaction().replace(R.id.sample_content_fragment, currentFragment,
                        ((BaseFragment) currentFragment).getTagFragment()).commit();
//        }
        /*else
        {
            currentFragment = (SlidingTabsFragment)mFragmentManager.findFragmentByTag(SlidingTabsFragment.TAG);
            if(currentFragment.getTabs() == null)
            {
                currentFragment = (SlidingTabsFragment) initFragment();
            }
        }*/
        initView();
        tvLog = (TextView) view.findViewById(R.id.tvLog);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    protected boolean justCallSuper() {
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem logToggle = menu.findItem(R.id.menu_toggle_log);
        logToggle.setVisible(findViewById(R.id.sample_output) instanceof ViewAnimator);
        logToggle.setTitle(mLogShown ? R.string.title_menu_hide_log : R.string.title_menu_show_log);

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.menu_toggle_log:
                mLogShown = !mLogShown;
                ViewAnimator output = (ViewAnimator) findViewById(R.id.sample_output);
                if (mLogShown) {
                    output.setDisplayedChild(1);
                } else {
                    output.setDisplayedChild(0);
                }
                supportInvalidateOptionsMenu();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }



    /* *//** Create a chain of targets that will receive log data *//*
    @Override
    public void initializeLogging() {
        // Wraps Android's native log framework.
        LogWrapper logWrapper = new LogWrapper();
        // Using Log, front-end to the logging chain, emulates android.util.log method signatures.
        Log.setLogNode(logWrapper);

        // Filter strips out everything except the message text.
        MessageOnlyLogFilter msgFilter = new MessageOnlyLogFilter();
        logWrapper.setNext(msgFilter);

        // On screen logging via a fragment with a TextView.
        LogFragment logFragment = (LogFragment) getSupportFragmentManager()
                .findFragmentById(R.id.log_fragment);
        msgFilter.setNext(logFragment.getLogView());

        Log.i(TAG, "Ready");
    }*/
}
