package app.dzumi.demot3h.demo.sqlite.contentprovider.base;

/**
 * Created by dzumi on 16/05/2015.
 */
public class Country extends DBContract.Country{


    int _id;
    String nameEn;
    String nameVi;
    int flagID;
    String flag;
    String caption;

    public long getPopulation() {
        return population;
    }

    public void setPopulation(long population) {
        this.population = population;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    long population;

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameVi() {
        return nameVi;
    }

    public void setNameVi(String nameVi) {
        this.nameVi = nameVi;
    }

    public int getFlagID() {
        return flagID;
    }

    public void setFlagID(int flagID) {
        this.flagID = flagID;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public Country(String nameEn, String nameVi, int flagID) {
        this.nameEn = nameEn;
        this.nameVi = nameVi;
        this.flagID = flagID;
    }

    public Country() {
    }
}
