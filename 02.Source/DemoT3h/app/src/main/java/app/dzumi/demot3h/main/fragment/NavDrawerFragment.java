package app.dzumi.demot3h.main.fragment;


import android.os.Bundle;
import android.widget.Toast;

import app.dzumi.demot3h.BaseActivity;
import app.dzumi.demot3h.R;
import lib.dzumi.nav.NavigationDrawerFragment;
import lib.dzumi.nav.element.MaterialAccount;
import lib.dzumi.nav.element.MaterialSection;
import lib.dzumi.nav.element.listeners.MaterialSectionListener;

/**
 * Created by dzumi on 07/03/2015.
 */
public class NavDrawerFragment extends NavigationDrawerFragment implements MaterialSectionListener {
    private static final String TAG_LOGOUT = "TAG_LOGOUT";
    @Override
    public void init(Bundle savedInstanceState) {
        MaterialAccount account = new MaterialAccount(NavDrawerFragment.this.getResources(), getString(R.string.nav_name),
                getString(R.string.nav_phone));
        addAccount(account);

        //khai bao
        MaterialSection sectionLogout = newSection(getString(R.string.nav_logout), R.drawable.ic_exit, this, TAG_LOGOUT);
        addSection(sectionLogout);
    }

    @Override
    public void onClick(MaterialSection section) {
        String tag = section.getTag();
        if(tag.equals(TAG_LOGOUT))
        {
            Toast.makeText(getActivity(),getString(R.string.nav_logout), Toast.LENGTH_LONG ).show();
        }

        ((BaseActivity)getActivity()).closeNavigationDrawer();
    }
}
