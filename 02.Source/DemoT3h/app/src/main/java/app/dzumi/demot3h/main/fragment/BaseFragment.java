package app.dzumi.demot3h.main.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by dzumi on 07/03/2015.
 */
public abstract class BaseFragment extends Fragment {
    public final static String ITEMS = "items";

    private View root;
    protected Context mContext;
    protected Handler mHandler;
    protected abstract int layoutID();
    protected abstract void initViews(View root, Bundle savedInstanceState);
    protected abstract void setupViews();
    public abstract String getTagFragment();

    public  <V extends View> V findView( int viewId) {
        return (V) root.findViewById(viewId);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new Handler();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(layoutID(), container, false);
        initViews(root, savedInstanceState);
        setupViews();
        return root;
    }
}
