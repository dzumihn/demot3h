package app.dzumi.demot3h.main.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import app.dzumi.demot3h.R;

/**
 * Created by dzumi on 01/05/2015.
 */
public class ListViewFragment extends BaseFragment {
    static final String TAG = "ListViewFragment";
            ListView listView;

    public static ListViewFragment newInstance(ArrayList<String> items)
    {
        ListViewFragment fragment = new ListViewFragment();
        Bundle bundle = new Bundle();
        bundle.putStringArrayList(ITEMS, items);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    protected int layoutID() {
        return R.layout.layout_listview;
    }

    @Override
    protected void initViews(View root, Bundle savedInstanceState) {
        listView = findView(R.id.listview);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mItems = getArguments().getStringArrayList(ITEMS);
    }

    ArrayList<String> mItems;
    @Override
    protected void setupViews() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, mItems);
        listView.setAdapter(adapter);
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }
}
