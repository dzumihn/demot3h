package app.dzumi.demot3h.demo.adapter;

import android.app.Fragment;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.GridView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import app.dzumi.demot3h.BaseActivity;
import app.dzumi.demot3h.R;
import app.dzumi.demot3h.demo.adapter.adapter.CountryAdapter;
import app.dzumi.demot3h.demo.adapter.adapter.CountryAdapter2;
import app.dzumi.demot3h.demo.adapter.adapter.SampleAdapter1;
import app.dzumi.demot3h.demo.adapter.model.Country;

/**
 * Created by dzumi on 15/05/2015.
 */
public class SampleOtherAdapterViewActivity extends BaseActivity {
    GridView gridView;
    Spinner spinner;
    AutoCompleteTextView autoCompleteTextView;
    MultiAutoCompleteTextView multiAutoCompleteTextView;

    String[] mCountries;
    List<Country> mCountryList;
    @Override
    protected Fragment initFragment() {
        return null;
    }

    @Override
    protected View initContentView() {
        return getView(R.layout.activity_adapter_other_adapter_view);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData();

        gridView = (GridView) findViewById(R.id.gridView);
        spinner = (Spinner) findViewById(R.id.spinner2);
        autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
        multiAutoCompleteTextView = (MultiAutoCompleteTextView) findViewById(R.id.multiAutoCompleteTextView);


        //tao adapter
        //adapter default
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.item_list_view_sample1, mCountries );
       /*  ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, mCountries );*/

        //custom adapter extend base Adapter, only textview
        SampleAdapter1 sampleAdapter1 = new SampleAdapter1(this, R.layout.item_list_view_sample2, mCountries);
        CountryAdapter countryAdapter = new CountryAdapter(this, R.layout.item_list_view_countries1, mCountryList);
        CountryAdapter2 countryAdapter2 = new CountryAdapter2(this, R.layout.item_list_view_countries1, mCountryList);

        autoCompleteTextView.setAdapter(adapter);

        multiAutoCompleteTextView.setAdapter(adapter);
        multiAutoCompleteTextView.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

        spinner.setAdapter(countryAdapter2);
        gridView.setAdapter(sampleAdapter1);

    }

    void initData()
    {
        mCountries = getResources().getStringArray(R.array.countries_vi);

        String[] arrayNameVi =getResources().getStringArray(R.array.countries_vi);
        String[] arrayNameEn =getResources().getStringArray(R.array.countries_en);
//        TypedArray arrayFlag = getResources().obtainTypedArray(R.array.flags);
        int[] flags = {R.drawable.cambodia,R.drawable.canada,
                R.drawable.united_states,R.drawable.argentina,
                R.drawable.poland,R.drawable.turkey,
                R.drawable.vietnam,R.drawable.italy,
                R.drawable.england,R.drawable.spain,
                R.drawable.china,R.drawable.thailand,
                R.drawable.germany,R.drawable.korea,
                R.drawable.japan};

        mCountryList = new ArrayList<>();
        for(int i = 0; i < arrayNameEn.length; i++)
        {
            Country country = new Country(arrayNameEn[i], arrayNameVi[i],
                    flags[i]);
            mCountryList.add(country);
        }
    }
}
