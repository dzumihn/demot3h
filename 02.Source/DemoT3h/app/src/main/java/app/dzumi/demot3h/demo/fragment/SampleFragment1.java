package app.dzumi.demot3h.demo.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import app.dzumi.demot3h.R;

/**
 * Created by dzumi on 25/05/2015.
 */
public class SampleFragment1 extends Fragment {

    TextView tvTitle;
    Button button;

    String title;
    int layoutId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        title = bundle.getString("title", "Unknown");
        layoutId = bundle.getInt("layoutID", -1);
        tag = bundle.getString("tag", "tagUnknown");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = null;

        if(layoutId == -1) //co truyen vao layoutID khong? --> khong --> default
            rootView = inflater.inflate(R.layout.fragment_sample1, null);
        else
            rootView = inflater.inflate(layoutId, null);

        tvTitle = (TextView) rootView.findViewById(R.id.tvTitle);
        button = (Button) rootView.findViewById(R.id.button);

        tvTitle.setText(title);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), title, Toast.LENGTH_SHORT).show();
            }
        });
        return rootView;
    }

    String tag;

    public String getTagFragment()
    {
        return tag;
    }

}
