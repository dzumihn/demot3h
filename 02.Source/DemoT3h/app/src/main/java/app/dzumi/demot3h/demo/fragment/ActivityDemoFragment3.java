package app.dzumi.demot3h.demo.fragment;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import app.dzumi.demot3h.R;

/**
 * Created by dzumi on 25/05/2015.
 */
public class ActivityDemoFragment3 extends Activity implements View.OnClickListener{

    Button btnFragment1, btnFragment2, btnFragment3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_fragment);
        btnFragment1 = (Button) findViewById(R.id.btnFragment1);
        btnFragment2 = (Button) findViewById(R.id.btnFragment2);
        btnFragment3 = (Button) findViewById(R.id.btnFragment3);

        btnFragment1.setOnClickListener(this);
        btnFragment2.setOnClickListener(this);
        btnFragment3.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        Bundle bundle = new Bundle();
        SampleFragment1 fragment = null;
        if(id == R.id.btnFragment1)
        {
            //load fragment 1
            fragment = new SampleFragment1();
            bundle.putString("title", "Fragment 1");
            bundle.putString("tag", "fragment1");

        }else if(id == R.id.btnFragment2)
        {
            //load fragment 2
            fragment = new SampleFragment1();
            bundle.putString("title", "Fragment 2");
            bundle.putInt("layoutID", R.layout.fragment_sample2);
            bundle.putString("tag", "fragment2");

        }else if(id == R.id.btnFragment3)
        {
            //load fragment 3
            fragment = new SampleFragment1();
//            bundle.putString("title", "Fragment 3");
            bundle.putString("tag", "fragment3");
        }

        fragment.setArguments(bundle);
        pushFragment(fragment, true);
    }

    void pushFragment(SampleFragment1 fragment, boolean isAddToBackStack)
    {
        FragmentManager fragmentManager = getFragmentManager();
        //bat dau
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        //add vao dau & add cai gi
        transaction.replace(R.id.container, fragment);

        if(isAddToBackStack)
            transaction.addToBackStack(fragment.getTagFragment());

        //ket thuc
        transaction.commit();
    }
}
