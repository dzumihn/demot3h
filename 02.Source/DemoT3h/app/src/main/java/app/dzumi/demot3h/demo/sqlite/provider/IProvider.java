package app.dzumi.demot3h.demo.sqlite.provider;

import java.util.List;

import app.dzumi.demot3h.demo.sqlite.model.Country;

/**
 * Created by dzumi on 01/06/2015.
 */
public interface IProvider {
    List<Country> get();
    long insert(Country country);

}
