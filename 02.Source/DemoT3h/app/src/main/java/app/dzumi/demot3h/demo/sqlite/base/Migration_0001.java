package app.dzumi.demot3h.demo.sqlite.base;

import app.dzumi.demot3h.demo.sqlite.model.Country;

public class Migration_0001 extends Migration {
    public Migration_0001() {
        steps.add("CREATE TABLE " + Country.TABLE_NAME
                + " (" + Country._ID + " INTEGER PRIMARY KEY, "
                + Country.NAME_EN + " TEXT, "
                + Country.NAME_VI + " TEXT, "
                + Country.FLAG + " TEXT, "
                + Country.POPULATION + " REAL, "
                + Country.CAPTION + " TEXT)");

    }
}