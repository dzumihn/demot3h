package app.dzumi.demot3h.demo.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import app.dzumi.demot3h.BaseActivity;
import app.dzumi.demot3h.R;

/**
 * Created by dzumi on 25/05/2015.
 */
public class ActivityDemoFragment extends BaseActivity implements View.OnClickListener{
    @Override
    protected Fragment initFragment() {
        return null;
    }

    @Override
    protected View initContentView() {
        return getView(R.layout.activity_demo_fragment);
    }

    Button btnFragment1, btnFragment2, btnFragment3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        btnFragment1 = (Button) findViewById(R.id.btnFragment1);
        btnFragment2 = (Button) findViewById(R.id.btnFragment2);
        btnFragment3 = (Button) findViewById(R.id.btnFragment3);

        btnFragment1.setOnClickListener(this);
        btnFragment2.setOnClickListener(this);
        btnFragment3.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        Bundle bundle = new Bundle();
        SampleFragment1 fragment = null;
        if(id == R.id.btnFragment1)
        {
            //load fragment 1
            fragment = new SampleFragment1();
            bundle.putString("title", "Fragment 1");

        }else if(id == R.id.btnFragment2)
        {
            //load fragment 2
            fragment = new SampleFragment1();
            bundle.putString("title", "Fragment 2");
            bundle.putInt("layoutID", R.layout.fragment_sample2);

        }else if(id == R.id.btnFragment3)
        {
            //load fragment 3
            fragment = new SampleFragment1();
//            bundle.putString("title", "Fragment 3");

        }

        fragment.setArguments(bundle);
        pushFragment(fragment);
    }

    void pushFragment(Fragment fragment)
    {
        FragmentManager fragmentManager = getFragmentManager();
        //bat dau
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        //add vao dau & add cai gi
        transaction.add(R.id.container, fragment );

        //ket thuc
        transaction.commit();
    }
}
