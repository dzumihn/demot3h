package app.dzumi.demot3h.demo.broadcast_receiver;

import android.app.Fragment;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import app.dzumi.demot3h.BaseActivity;
import app.dzumi.demot3h.R;

/**
 * Created by dzumi on 10/07/2015.
 */
public class ActivitySampleReceiver extends BaseActivity {

    @Override
    protected Fragment initFragment() {
        return null;
    }

    @Override
    protected View initContentView() {
        return getView(R.layout.activity_sample_receiver);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        networkReceiver = new NetworkReceiver();
        batteryChangedReceiver = new BatteryChangedReceiver();
    }

    BatteryChangedReceiver batteryChangedReceiver;
    IntentFilter batteryChangedIntentFilter;

    NetworkReceiver networkReceiver;
    IntentFilter networkIntentFilter;
    @Override
    protected void onResume() {
        super.onResume();
        networkIntentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(networkReceiver, networkIntentFilter);

        batteryChangedIntentFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        registerReceiver(batteryChangedReceiver, batteryChangedIntentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(networkReceiver);
        unregisterReceiver(batteryChangedReceiver);
    }
}
