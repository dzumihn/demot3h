/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package app.dzumi.demot3h.main.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import app.dzumi.demot3h.MainPagerActivity;
import app.dzumi.demot3h.R;
import app.dzumi.demot3h.main.interfaces.OnPagerChanged;
import app.dzumi.demot3h.main.view.SlidingTabLayout;

/**
 * A basic sample which shows how to use {@link com.example.android.common.view.SlidingTabLayout}
 * to display a custom {@link ViewPager} title strip which gives continuous feedback to the user
 * when scrolling.
 */
public class SlidingTabsFragment extends BaseFragment {


    /**
     * This class represents a tab to be displayed by {@link ViewPager} and it's associated
     * {@link SlidingTabLayout}.
     */
    public static class PagerItem {
        private final CharSequence mTitle;
        private final int mIndicatorColor;
        private final int mDividerColor;
        String mLog;
        BaseFragment mFragment;

        public PagerItem(CharSequence title, int indicatorColor, int dividerColor) {
            mTitle = title;
            mIndicatorColor = indicatorColor;
            mDividerColor = dividerColor;
        }

        /**
         * @return the title which represents this tab. In this sample this is used directly by
         * {@link android.support.v4.view.PagerAdapter#getPageTitle(int)}
         */
        CharSequence getTitle() {
            return mTitle;
        }


        /**
         * @return the color to be used for indicator on the {@link SlidingTabLayout}
         */
        int getIndicatorColor() {
            return mIndicatorColor;
        }

        /**
         * @return the color to be used for right divider on the {@link SlidingTabLayout}
         */
        int getDividerColor() {
            return mDividerColor;
        }

        Fragment getFragment() {
            return mFragment;
        }
        String getLog(){return mLog;}
        public void setFragment(BaseFragment fragment) {
            mFragment = fragment;
        }
        public void setLog(String log){mLog = log;}
    }

    public static final String TAG = "SlidingTabsColorsFragment";

    /*public static SlidingTabsFragment newInstance(List<PagerItem> tabs)
    {
        SlidingTabsFragment fragment = new SlidingTabsFragment();
        fragment.mTabs = tabs;
        return fragment;
    }*/
    /**
     * A custom {@link ViewPager} title strip which looks much like Tabs present in Android v4.0 and
     * above, but is designed to give continuous feedback to the user when scrolling.
     */
    private SlidingTabLayout mSlidingTabLayout;

    /**
     * A {@link ViewPager} which will be used in conjunction with the {@link SlidingTabLayout} above.
     */
    private ViewPager mViewPager;

    OnPagerChanged onPagerChanged;
    MainPagerActivity pagerActivity;

    @Override
    protected int layoutID() {
        return R.layout.fragment_tab_viewpager;
    }

    @Override
    protected void initViews(View root, Bundle savedInstanceState) {

    }

    @Override
    protected void setupViews() {
        pagerActivity = ((MainPagerActivity)getActivity());
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

    /**
     * This is called after the {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)} has finished.
     * Here we can pick out the {@link View}s we need to configure from the content view.
     * <p/>
     * We set the {@link ViewPager}'s adapter to be an instance of
     * {@link SampleFragmentPagerAdapter}. The {@link SlidingTabLayout} is then given the
     * {@link ViewPager} so that it can populate itself.
     *
     * @param view View created in {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // BEGIN_INCLUDE (setup_viewpager)
        // Get the ViewPager and set it's PagerAdapter so that it can display items
        mViewPager = (ViewPager) view.findViewById(R.id.viewpager);
        mViewPager.setAdapter(new MyFragmentPagerAdapter(getChildFragmentManager()));
        // END_INCLUDE (setup_viewpager)

        // BEGIN_INCLUDE (setup_slidingtablayout)
        // Give the SlidingTabLayout the ViewPager, this must be done AFTER the ViewPager has had
        // it's PagerAdapter set.
        mSlidingTabLayout = (SlidingTabLayout) view.findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setViewPager(mViewPager);

        // BEGIN_INCLUDE (tab_colorizer)
        // Set a TabColorizer to customize the indicator and divider colors. Here we just retrieve
        // the tab at the position, and return it's set color
        mSlidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {

            @Override
            public int getIndicatorColor(int position) {
                return pagerActivity.getTabs().get(position).getIndicatorColor();
            }

            @Override
            public int getDividerColor(int position) {

                return pagerActivity.getTabs().get(position).getDividerColor();
            }

        });
        // END_INCLUDE (tab_colorizer)
        // END_INCLUDE (setup_slidingtablayout)

        //thiet lap onPagerChanged --> khi swipe qua pager khac, hien thi log khac
        onPagerChanged = ((MainPagerActivity) getActivity());

        //thiet lap gia tri log default
        onPagerChanged.onPagerChanged(getString(R.string.mcc_mnc_demo_log));

        mSlidingTabLayout.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                onPagerChanged.onPagerChanged(pagerActivity.getTabs().get(position).getLog());
               /* switch (position) {
                    case 0: //mcc, mnc
                        onPagerChanged.onPagerChanged(getString(R.string.mcc_mnc_demo_log));
                        break;
                    case 1: //lang, region
                        onPagerChanged.onPagerChanged(getString(R.string.lang_region_demo_log));
                        break;
                    case 2: //layout direction
                        onPagerChanged.onPagerChanged(getString(R.string.layout_direction_demo_log));
                        break;
                    case 3://smallest width
                        onPagerChanged.onPagerChanged(getString(R.string.smallest_width_demo_log));
                        break;
                    case 4://screen size
                        onPagerChanged.onPagerChanged(getString(R.string.screen_size_demo_log));
                        break;
                    case 5://screen aspect
                        onPagerChanged.onPagerChanged(getString(R.string.screen_aspect_demo_log));
                        break;
                    case 6://screen orientation
                        onPagerChanged.onPagerChanged(getString(R.string.screen_orientation_demo_log));
                        break;
                    case 7://ui mode
                        onPagerChanged.onPagerChanged(getString(R.string.ui_mode_demo_log));
                        break;
                    case 8://night mode
                        onPagerChanged.onPagerChanged(getString(R.string.night_mode_demo_log));
                        break;
                    case 9://screen density
                        onPagerChanged.onPagerChanged(getString(R.string.screen_density_demo_log));
                        break;
                }*/
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
    // END_INCLUDE (fragment_onviewcreated)

    /**
     * The {@link FragmentPagerAdapter} used to display pages in this sample. The individual pages
     * are instances of {@link ContentFragment} which just display three lines of text. Each page is
     * created by the relevant {@link SamplePagerItem} for the requested position.
     * <p/>
     * The important section of this class is the {@link #getPageTitle(int)} method which controls
     * what is displayed in the {@link SlidingTabLayout}.
     */
    class MyFragmentPagerAdapter extends android.support.v13.app.FragmentPagerAdapter {

        public MyFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public android.app.Fragment getItem(int position) {
            return pagerActivity.getTabs().get(position).getFragment();
        }

        @Override
        public int getCount() {
            return pagerActivity.getTabs().size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return pagerActivity.getTabs().get(position).getTitle();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        String abc = "";
    }
}