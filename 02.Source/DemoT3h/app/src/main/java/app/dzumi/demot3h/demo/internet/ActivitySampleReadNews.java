package app.dzumi.demot3h.demo.internet;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import app.dzumi.demot3h.R;
import app.dzumi.demot3h.demo.internet.xml.News;
import app.dzumi.demot3h.demo.internet.xml.RSSUtils;
import app.dzumi.demot3h.main.utils.Log;

public class ActivitySampleReadNews extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_sample_read_news);
//        TextView textView = (TextView) findViewById(R.id.textView);
//        textView.setText(R.string.temp);
//        textView.setText(Html.fromHtml(getString(R.string.temp)));
        MyAsyntaskReadNews myAsyntaskReadNews = new MyAsyntaskReadNews();
        try {
            myAsyntaskReadNews.execute(new URL("http://www.24h.com.vn/upload/rss/bongda.rss"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_sample_read_news, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class MyAsyntaskReadNews extends AsyncTask<URL,Void,List<News>>{

        @Override
        protected List<News> doInBackground(URL... params) {
            //buoc 1: tao ket noi
            for(URL url : params)
            {
                try {
                    URLConnection urlConnection = url.openConnection();
                    if(url.getPath().contains("HTTPS"))
                    {
                        HttpsURLConnection httpsURLConnection = (HttpsURLConnection) urlConnection;
                        if(httpsURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK)
                        {
                            //lay inputStream va thuc hien parse du lieu
                            return RSSUtils.readRSS24h(httpsURLConnection.getInputStream());
                        }else{
                            //that bai
                        }
                    }else
                    {
                        HttpURLConnection httpURLConnection = (HttpURLConnection) urlConnection;
                        if(httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK)
                        {
                            return RSSUtils.readRSS24h(httpURLConnection.getInputStream());
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<News> newsList) {
//            super.onPostExecute(newses);
            if(newsList != null)
            {
                for(News news : newsList)
                {
                    Log.d(news.toString());
                }
            }
        }
    }
}
