package app.dzumi.demot3h.demo.sqlite.provider;

import android.content.Context;

import java.util.List;

import app.dzumi.demot3h.demo.sqlite.dao.CountryDAO;
import app.dzumi.demot3h.demo.sqlite.model.Country;

/**
 * Created by dzumi on 01/06/2015.
 */
public class Provider1 implements IProvider {

    CountryDAO countryDAO;
    public Provider1(Context context, String path)
    {
        countryDAO = new CountryDAO(context, path);
    }
    @Override
    public List<Country> get() {
        return countryDAO.get();
    }

    @Override
    public long insert(Country country) {
        return countryDAO.insertCountry(country);
    }
}
