package app.dzumi.demot3h.demo.contentprovider.base;

import android.database.Cursor;

public interface BaseAction {
    void clearAllData();
    Cursor rawQuery(String sql, String[] args);
}