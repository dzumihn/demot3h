package app.dzumi.demot3h;

import android.app.Application;

import app.dzumi.demot3h.demo.media_and_camera.ManagerInitializer;

/**
 * Created by dzumi on 20/05/2015.
 */
public class BaseApplication extends Application{
    @Override
    public void onCreate() {
        super.onCreate();
        ManagerInitializer.i.init(getApplicationContext());

    }
}
