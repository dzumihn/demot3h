/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Zillow
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package app.dzumi.demot3h.demo.media_and_camera;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;

import app.dzumi.demot3h.R;
import app.dzumi.demot3h.demo.media_and_camera.control.PinchImageView;
import app.dzumi.demot3h.main.fragment.BaseFragment;

public class PhotoPreviewFragment extends BaseFragment {
    public static final String TAG = "PhotoPreviewFragment";
    private Bitmap bitmap;
    private PinchImageView imageView;

    public static PhotoPreviewFragment newInstance(Bitmap bitmap) {
        PhotoPreviewFragment fragment = new PhotoPreviewFragment();
        fragment.bitmap = bitmap;

        return fragment;
    }

    @Override
    protected int layoutID() {
        return R.layout.fragment_photo_preview;
    }

    @Override
    protected void initViews(View root, Bundle savedInstanceState) {

    }

    @Override
    protected void setupViews() {

    }

    @Override
    public String getTagFragment() {
        return TAG;
    }


    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        imageView = (PinchImageView) view.findViewById(R.id.photo);

        if (bitmap != null && !bitmap.isRecycled()) {
            imageView.setImageBitmap(bitmap);
        } else {
            imageView.setImageResource(R.drawable.no_image);
        }
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
        imageView.setImageBitmap(bitmap);
    }

}
