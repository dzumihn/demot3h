package app.dzumi.demot3h.demo.contentprovider.contacts;

import android.provider.BaseColumns;

/**
 * Created by dzumi on 06/07/2015.
 */
public class MyContact implements BaseColumns{
    String name;
    String phone;
    String email;
    String address;

    public MyContact()
    {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "name:" + name + " /phone: " + phone + " /email: " + email + "/ address: " + address;
    }
}
