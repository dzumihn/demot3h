package app.dzumi.demot3h.demo.adapter.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import app.dzumi.demot3h.R;
import app.dzumi.demot3h.demo.adapter.model.Country;

/**
 * Created by dzumi on 16/05/2015.
 * Dung de hien thi danh sach ten quoc gia --> textView
 */
public class CountryAdapter extends BaseAdapter{
    Context mContext;
    int mLayoutID;
    List<Country> mCountries;
    public CountryAdapter(Context context, int layoutID, List<Country> countries)
    {
        mContext = context;
        mLayoutID = layoutID;
        mCountries = countries;
    }
    @Override
    public int getCount() {
        //chung ta phai tra ve so luong item can duoc tao ra tren listview
        return mCountries.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;

        //buoc 1: tu layoutID(R.layout.name) --> view
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);

        view = inflater.inflate(mLayoutID, null);

        //buoc 2: find view member trong view
        TextView tvNameVi = (TextView) view.findViewById(R.id.tvNameVi);
        TextView tvNameEn = (TextView) view.findViewById(R.id.tvNameEn);
        ImageView ivFlag = (ImageView) view.findViewById(R.id.ivFlag);

        //buoc 3: set info
        tvNameVi.setText(mCountries.get(position).getNameVi());
        tvNameEn.setText(mCountries.get(position).getNameEn());
        ivFlag.setImageResource(mCountries.get(position).getFlagID());
        return view;
    }
}
