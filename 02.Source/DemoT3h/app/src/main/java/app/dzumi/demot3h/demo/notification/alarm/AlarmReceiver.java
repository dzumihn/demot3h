package app.dzumi.demot3h.demo.notification.alarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AlarmReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		  Calendar now = Calendar.getInstance();
		  DateFormat formatter = SimpleDateFormat.getTimeInstance();
	     Toast.makeText(context, formatter.format(now.getTime()), Toast.LENGTH_SHORT).show();
	}

}
