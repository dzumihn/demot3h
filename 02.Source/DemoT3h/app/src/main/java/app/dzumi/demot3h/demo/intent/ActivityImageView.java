package app.dzumi.demot3h.demo.intent;

import android.app.Fragment;
import android.os.Bundle;
import android.view.View;

import app.dzumi.demot3h.BaseActivity;
import app.dzumi.demot3h.main.fragment.ImageFragment;

/**
 * Created by dzumi on 29/05/2015.
 */
public class ActivityImageView extends BaseActivity {
    @Override
    protected Fragment initFragment() {
        ImageFragment imageFragment = new ImageFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(ImageFragment.EXTRA_PATH, getIntent().getData());
//        bundle.putString(ImageFragment.EXTRA_PATH, getIntent().getStringExtra(ImageFragment.EXTRA_PATH));
        imageFragment.setArguments(bundle);
        return imageFragment;
    }

    @Override
    protected View initContentView() {
        return null;
    }
}
