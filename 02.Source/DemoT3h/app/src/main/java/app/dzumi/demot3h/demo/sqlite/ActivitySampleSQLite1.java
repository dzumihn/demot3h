package app.dzumi.demot3h.demo.sqlite;

import android.app.Activity;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.io.IOException;

import app.dzumi.demot3h.R;
import app.dzumi.demot3h.demo.sqlite.adapter.CountryAdapter;
import app.dzumi.demot3h.demo.sqlite.provider.IProvider;
import app.dzumi.demot3h.demo.sqlite.provider.Provider1;
import app.dzumi.demot3h.demo.sqlite.provider.Provider2;
import app.dzumi.demot3h.main.utils.IOUtil;

/**
 * Created by dzumi on 01/06/2015.
 */
public class ActivitySampleSQLite1 extends Activity implements View.OnClickListener{
    ListView listView;
    Button btnExternalDB, btnInternalDB, btnAssetDB;
    CountryAdapter countryAdapter;
    IProvider iProvider;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_sqlite);
        listView = (ListView) findViewById(R.id.listView);
        btnAssetDB = (Button) findViewById(R.id.btnAssetDB);
        btnExternalDB = (Button) findViewById(R.id.btnExternalDB);
        btnInternalDB = (Button) findViewById(R.id.btnInternalDB);

        btnAssetDB.setOnClickListener(this);
        btnExternalDB.setOnClickListener(this);
        btnInternalDB.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if(id == R.id.btnExternalDB)
        {
            String path = Environment.getExternalStorageDirectory() +
                    "/Download/DBCountry";
            iProvider = new Provider1(this, path);
        }else if(id == R.id.btnInternalDB)
        {
            iProvider = new Provider2(this);
        }else if(id == R.id.btnAssetDB)
        {
            String name = "DBCountry";
            String path =  getFileStreamPath(name).getAbsolutePath();
            IOUtil ioUtil =  new IOUtil(this);
            if(!ioUtil.internalFileExistance(name)) //chua ton tai thi copy file
            {
                AssetManager assetManager = getAssets();
                try {
                    ioUtil.copyFileToInternal(assetManager.open(name), name);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            iProvider = new Provider1(this, path);
        }

        countryAdapter = new CountryAdapter(this, R.layout.item_list_view_countries1, iProvider.get());
        listView.setAdapter(countryAdapter);
    }
}
