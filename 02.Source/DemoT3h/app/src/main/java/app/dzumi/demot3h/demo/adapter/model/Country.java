package app.dzumi.demot3h.demo.adapter.model;

/**
 * Created by dzumi on 16/05/2015.
 */
public class Country {
    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameVi() {
        return nameVi;
    }

    public void setNameVi(String nameVi) {
        this.nameVi = nameVi;
    }

    public int getFlagID() {
        return flagID;
    }

    public void setFlagID(int flagID) {
        this.flagID = flagID;
    }
    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }
    String nameEn;
    String nameVi;
    int flagID;



    String capital = "Midu";
    public Country(String nameEn, String nameVi, int flagID) {
        this.nameEn = nameEn;
        this.nameVi = nameVi;
        this.flagID = flagID;
    }
}
