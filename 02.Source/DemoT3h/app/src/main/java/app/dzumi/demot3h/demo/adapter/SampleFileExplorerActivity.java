package app.dzumi.demot3h.demo.adapter;

import android.app.Fragment;
import android.os.Bundle;
import android.app.Activity;
import android.os.Environment;
import android.view.Menu;
import android.view.View;
import android.widget.*;

import java.io.File;
import java.util.ArrayList;

import app.dzumi.demot3h.BaseActivity;
import app.dzumi.demot3h.R;

public class SampleFileExplorerActivity extends BaseActivity implements AdapterView.OnItemClickListener {

    ListView listView;
    TextView tvPath;
    String mPath;

    ArrayList<String> mFileNames;
    ArrayList<String> mFilePath;

    @Override
    protected Fragment initFragment() {
        return null;
    }

    @Override
    protected View initContentView() {
        return getView(R.layout.activity_adapter_file_explorer);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        listView = (ListView)findViewById(R.id.listView);
        listView.setOnItemClickListener(this);
        mPath = Environment.getExternalStorageDirectory().getPath();
        tvPath = (TextView)findViewById(R.id.textPath);

        getDirectory(mPath);
    }

    public void getDirectory(String pathDir){

        tvPath.setText(pathDir);

        mFileNames = new ArrayList<String>();
        mFilePath = new ArrayList<String>();

        File dir = new File(pathDir);

        mFileNames.add("../");
        mFilePath.add(dir.getParent());

        if(dir.isDirectory()){
            File[] files = dir.listFiles();
            for(int i=0; i<files.length; i++){
                mFileNames.add(files[i].getName());
                mFilePath.add(files[i].getAbsolutePath());
            }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mFileNames);
        listView.setAdapter(adapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        File file = new File(mFilePath.get(i));
        if(file.isDirectory() && !file.isHidden() && file.canRead()){
            getDirectory(file.getAbsolutePath());
        } else {
            Toast.makeText(this, file.getName(), Toast.LENGTH_SHORT).show();
        }
    }
}
