package app.dzumi.demot3h.demo.xml_json;

import android.content.Context;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import app.dzumi.demot3h.main.utils.Log;

/**
 * Created by dzumi on 17/07/2015.
 */
public class XMLUtils {

    public static List<Employee> parseEmployeesWithSAX(Context context, int file) {
        List<Employee> employees = new ArrayList<>();
        //doc file xml trong thu muc res/xml bang xmlPullParser
        XmlPullParser xmlPullParser = context.getResources().getXml(file);
        int eventType = -1;
        try {
            while (eventType != XmlPullParser.END_DOCUMENT) {

                eventType = xmlPullParser.next();
                if(eventType == XmlPullParser.START_DOCUMENT)
                {
                    Log.d("START_DOCUMENT");
                }else if(eventType == XmlPullParser.END_DOCUMENT)
                {
                    Log.d("END_DOCUMENT" );
                }
                else if(eventType == XmlPullParser.START_TAG)
                {
                    //Xuat ra man hinh TAG_NAME
                    Log.d("START_TAG: "  + xmlPullParser.getName());

                    //muon gan gia tri vao model
                    String name = xmlPullParser.getName();
                    if(name.equals(Employee.EMPOLYEE))
                    {
                        //chung ta khong bik co bao nhieu attribute trong start_tag
                        int count = xmlPullParser.getAttributeCount();

                        for(int i = 0; i < count; i++)
                        {
                            Log.d("Attribute: " + xmlPullParser.getAttributeName(i)+"/"+
                                    xmlPullParser.getAttributeValue(i));
                        }
                    }
                    else if(name.equals(Employee.FNAME))
                    {

                    }
                    else if(name.equals(Employee.LNAME))
                    {

                    }
                }
                else if(eventType == XmlPullParser.END_TAG)
                {
                    Log.d("END_TAG: " + xmlPullParser.getName());
                }
                else if(eventType == XmlPullParser.TEXT)
                {
                    Log.d("TEXT: " + xmlPullParser.getText());
                }
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return employees;
    }
    public static List<Employee> parseEmployeeWithDOM(Context context, int file)
    {
        List<Employee> employees = new ArrayList<>();
        DocumentBuilderFactory builderFactory =
                DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = null;
        try {
            documentBuilder = builderFactory.newDocumentBuilder();
            Document document =
                    documentBuilder.parse(context.getResources().openRawResource(file));
            //lay danh sach employee
            if(document!= null){
                NodeList listEmployee =
                        document.getElementsByTagName(Employee.EMPOLYEE);
                if(listEmployee != null)
                {
                    for (int i = 0; i < listEmployee.getLength(); i++)
                    {
                        Employee employee = new Employee();
                        Element elementEmployee = (Element) listEmployee.item(i);

                        employee.setId(Integer.parseInt(elementEmployee.getAttribute(Employee.ID)));

                        //lay danh sach node con cua employee
                        NodeList childrent = elementEmployee.getChildNodes();
                        if(childrent != null)
                        {
                            for(int j = 0; j < childrent.getLength(); j++)
                            {
                                Node child = childrent.item(j);
                                String name = child.getNodeName();
                                String textContent = child.getTextContent();
                                if(name.equals(Employee.FNAME))
                                {

                                    employee.setFName(textContent);
                                }else if(name.equals(Employee.LNAME))
                                {
                                    employee.setLName(textContent);
                                }
                            }
                        }
                    }
                }
            }

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return employees;
    }
}
