package app.dzumi.demot3h.demo.intent;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import app.dzumi.demot3h.BaseActivity;
import app.dzumi.demot3h.R;
import app.dzumi.demot3h.main.dialog.DialogInfo;
import app.dzumi.demot3h.main.dialog.IActionDialogInfo;

/**
 * Created by dzumi on 29/05/2015.
 */
public class ActivityIntentSampleStartForResult extends BaseActivity
        implements View.OnClickListener, IActionDialogInfo {
    public static final int REQUEST_CALCULATE = 111;
    public static final int REQUEST_CONFIRM_CALCULATE = 112;
    @Override
    protected Fragment initFragment() {
        return null;
    }

    @Override
    protected View initContentView() {
        return getView(R.layout.activity_intent_sample_start_for_result);
    }

    Button btnCalculate;
    EditText edtX, edtY, edtKQ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        btnCalculate = (Button) findViewById(R.id.btnCalculate);
        btnCalculate.setOnClickListener(this);
        edtKQ = (EditText) findViewById(R.id.edtKQ);
        edtX = (EditText) findViewById(R.id.edtX);
        edtY = (EditText) findViewById(R.id.edtY);

        edtKQ.setEnabled(false);
    }

    public final static int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1;
    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btnCalculate) {
            //validate value
            String x = edtX.getText().toString();
            String y = edtY.getText().toString();
            if(x.isEmpty() || y.isEmpty())
            {
                Toast.makeText(this, "Vui long nhap gia tri vao X hoac Y",
                        Toast.LENGTH_SHORT).show();
            }else if(!isDigit(x) || !isDigit(y))
            {
                Toast.makeText(this, "Vui long nhap gia tri So vao X hoac Y",
                        Toast.LENGTH_SHORT).show();
            }else
            {
                Bundle bundle = new Bundle();
                bundle.putString(DialogInfo.DIALOG_CONTENT, "Ban co muon thuc hien phep tinh khong?");
                bundle.putString(DialogInfo.DIALOG_TITLE, "Thong bao");
                bundle.putInt(DialogInfo.DIALOG_TYPE, DialogInfo.DIALOG_TYPE_YES_NO);
                DialogInfo dialogInfo = DialogInfo.newInstance(this,bundle, "Co", "Khong");
                dialogInfo.show(getFragmentManager(), "dialog");
            }
        }
    }

    boolean isDigit(String s)
    {
        int n = s.length();
        for(int i = 0; i < n; i++)
        {
            if(!Character.isDigit(s.charAt(i)))
                return false;
        }
        return true;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CALCULATE)
        {
            if(resultCode == RESULT_OK)
            {
                Bundle bundle = data.getExtras();
                double kq = bundle.getDouble(ActivityIntentSampleReturnResult.EXTRA_KQ);
                edtKQ.setText(kq + "");
            }else if(resultCode == RESULT_CANCELED)
            {
                if(data != null) {
                    Bundle bundle = data.getExtras();
                    String error = bundle.getString(ActivityIntentSampleReturnResult.EXTRA_ERROR);
                    edtKQ.setText(error);
                }
            }
        }
    }

    @Override
    public void onPositiveButtonClick() {
        Intent intent = new Intent(this, ActivityIntentSampleReturnResult.class);

        Bundle bundle = new Bundle();
        String x = edtX.getText().toString();
        String y = edtY.getText().toString();
        bundle.putDouble(ActivityIntentSampleReturnResult.EXTRA_X, Double.parseDouble(x));
        bundle.putDouble(ActivityIntentSampleReturnResult.EXTRA_Y, Double.parseDouble(y));

        intent.putExtras(bundle);

        startActivityForResult(intent, REQUEST_CALCULATE );
    }

    @Override
    public void onNegativeButtonClick() {

    }
}
