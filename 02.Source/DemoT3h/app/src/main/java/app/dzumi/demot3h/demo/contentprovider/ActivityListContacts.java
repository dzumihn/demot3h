package app.dzumi.demot3h.demo.contentprovider;

import android.app.Fragment;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import java.util.List;

import app.dzumi.demot3h.BaseActivity;
import app.dzumi.demot3h.R;
import app.dzumi.demot3h.demo.contentprovider.contacts.ContactDAO;
import app.dzumi.demot3h.demo.contentprovider.contacts.MyContact;
import app.dzumi.demot3h.main.utils.Log;

public class ActivityListContacts extends BaseActivity {

    ListView listView;
    ContactDAO mContactDAO;
    List<MyContact> myContactList;
    @Override
    protected Fragment initFragment() {
        return null;
    }

    @Override
    protected View initContentView() {
        return getView(R.layout.activity_list_contacts);
    }

    void initData()
    {
        mContactDAO = new ContactDAO(this);
        myContactList = mContactDAO.get();

        for(MyContact contact : myContactList)
            Log.d(contact.toString());

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData();
        listView = (ListView) findViewById(R.id.listView);

        //viet adapter

    }


    //<editor-fold desc="code menu">
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_list_contacts, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    //</editor-fold>
}
