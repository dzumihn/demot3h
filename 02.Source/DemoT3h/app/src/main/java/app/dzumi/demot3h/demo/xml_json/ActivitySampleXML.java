package app.dzumi.demot3h.demo.xml_json;

import android.app.Fragment;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import app.dzumi.demot3h.BaseActivity;
import app.dzumi.demot3h.R;

public class ActivitySampleXML extends BaseActivity implements View.OnClickListener{

    @Override
    protected Fragment initFragment() {
        return null;
    }

    @Override
    protected View initContentView() {
        return getView(R.layout.activity_activity_sample_xml);
    }

    Button btnParseXML_SAX, btnParseXML_DOM;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        btnParseXML_DOM = (Button) findViewById(R.id.btnParseXMLDOM);
        btnParseXML_SAX = (Button) findViewById(R.id.btnParseXMLSAX);

        btnParseXML_DOM.setOnClickListener(this);
        btnParseXML_SAX.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_sample_xml, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if(id == R.id.btnParseXMLSAX)
        {
            XMLUtils.parseEmployeesWithSAX(this, R.xml.employee);
        }else if(id == R.id.btnParseXMLDOM)
        {

        }
    }
}
