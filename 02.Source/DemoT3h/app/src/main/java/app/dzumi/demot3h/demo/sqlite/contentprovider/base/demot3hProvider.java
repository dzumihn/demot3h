package app.dzumi.demot3h.demo.sqlite.contentprovider.base;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.text.TextUtils;

import java.util.List;

public class demot3hProvider extends ContentProvider implements BaseAction {

    // helper constants for use with the UriMatcher
    private static final int COUNTRY_LIST = 1;
    private static final int COUNTRY_ID = 2;

    private static final int ALL = 100;
    private static final int GoToFinish = 101;
    private static final UriMatcher URI_MATCHER;

    static {
        URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
        URI_MATCHER.addURI(DBContract.AUTHORITY, "rawQuery", ALL);

        URI_MATCHER.addURI(DBContract.AUTHORITY, DBContract.Country.TABLE_NAME, COUNTRY_LIST);
        URI_MATCHER.addURI(DBContract.AUTHORITY, DBContract.Country.TABLE_NAME_ID, COUNTRY_ID);
    }

    DBContext mDbContext;
    private final ThreadLocal<Boolean> mIsInBatchMode = new ThreadLocal<Boolean>();

    //<editor-fold desc="main method">
    @Override
    public boolean onCreate() {

        mDbContext = new DBContext(getContext());
        return true;

    }


    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        doAnalytics(uri, "query");

        SQLiteDatabase db = mDbContext.getReadableDatabase();
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        boolean useAuthorityUri = false;

        switch (URI_MATCHER.match(uri)) {
            case COUNTRY_LIST:
                builder.setTables(DBContract.Country.TABLE_NAME);
                if (TextUtils.isEmpty(sortOrder)) {
                    sortOrder = DBContract.Country.SORT_ORDER_DEFAULT;
                }
                break;
            case COUNTRY_ID:
                builder.setTables(DBContract.Country.TABLE_NAME);
                // limit query to one row at most:
                builder.appendWhere(DBContract.Country._ID + " = "
                        + uri.getLastPathSegment());
                break;

            case ALL:
                return rawQuery(selection, selectionArgs);
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
        // if you like you can log the query
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            logQuery(builder, projection, selection, sortOrder);
        } else {
            logQueryDeprecated(builder, projection, selection, sortOrder);
        }

        Cursor cursor = builder.query(db, projection, selection, selectionArgs,
                null, null, sortOrder);

        // if we want to be notified of any changes:
        if (useAuthorityUri) {
            cursor.setNotificationUri(getContext().getContentResolver(), DBContract.CONTENT_URI);
        } else {
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        }
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        switch (URI_MATCHER.match(uri)) {
            case COUNTRY_LIST:
                return DBContract.Country.CONTENT_TYPE;
            case COUNTRY_ID:
                return DBContract.Country.CONTENT_ITEM_TYPE;

            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        doAnalytics(uri, "insert");
        SQLiteDatabase db = mDbContext.getWritableDatabase();
        long id;

        String table = "";
        switch (URI_MATCHER.match(uri)) {

            case COUNTRY_LIST:
                table = DBContract.Country.TABLE_NAME;
                break;
            default:
                throw new IllegalArgumentException(
                        "Unsupported URI for insertion: " + uri);
        }

        id = db.insert(table, null, values);
        return getUriForId(id, uri);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        doAnalytics(uri, "delete");

        SQLiteDatabase db = mDbContext.getWritableDatabase();
        int delCount = 0;
        String idStr;
        String where = null;
        String table = null;

        //case delete 1 item
        int goTo = URI_MATCHER.match(uri);
        boolean isFinish = false;
        while (!isFinish) {
            switch (goTo) {
                case GoToFinish:
                    if (!TextUtils.isEmpty(selection)) {
                        where += " AND " + selection;
                    }
                    delCount = db.delete(table, where, selectionArgs);
                    isFinish = true;
                    break;
                case COUNTRY_ID:
                    idStr = uri.getLastPathSegment();
                    where = DBContract.Country._ID + " = " + idStr;
                    table = DBContract.Country.TABLE_NAME;
                    goTo = GoToFinish;
                    continue;

                default:
                    isFinish = true;
                    break;
            }
        }

        switch (URI_MATCHER.match(uri)) {
            case COUNTRY_LIST:
                delCount = db.delete(DBContract.Country.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                // no support for deleting photos or entities -
                // photos are deleted by a trigger when the item is deleted
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        // notify all listeners of changes:
        if (delCount > 0 && !isInBatchMode()) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return delCount;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        doAnalytics(uri, "update");
        SQLiteDatabase db = mDbContext.getWritableDatabase();
        int updateCount = 0;

        //case update 1 item
        String idStr;
        String where = "";
        String table = "";
        int goTo = URI_MATCHER.match(uri);
        boolean isFinish = false;
        while (!isFinish) {
            switch (goTo) {
                case GoToFinish:
                    if (!TextUtils.isEmpty(selection)) {
                        where += " AND " + selection;
                    }
                    updateCount = db.update(table, values, where,selectionArgs);
                    isFinish = true;
                    break;

                case COUNTRY_ID:
                    idStr = uri.getLastPathSegment();
                    where = DBContract.Country._ID + " = " + idStr;
                    table = DBContract.Country.TABLE_NAME;
                    goTo = GoToFinish;
                    continue;

                default:
                    isFinish = true;
                    break;
            }
        }

        switch (URI_MATCHER.match(uri)) {
            case COUNTRY_LIST:
                updateCount = db.update(DBContract.Country.TABLE_NAME, values, selection, selectionArgs);
                break;
            default:
                // no support for deleting photos or entities -
                // photos are deleted by a trigger when the item is deleted
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        // notify all listeners of changes:
        if (updateCount > 0 && !isInBatchMode()) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return updateCount;
    }


    //</editor-fold>


    //<editor-fold desc="private functions">
    private Uri getUriForId(long id, Uri uri) {
        if (id > 0) {
            Uri itemUri = ContentUris.withAppendedId(uri, id);
            if (!isInBatchMode()) {
                // notify all listeners of changes and return itemUri:
                getContext().
                        getContentResolver().
                        notifyChange(itemUri, null);
            }
            return itemUri;
        }
        // s.th. went wrong:
//        throw new SQLException("Problem while inserting into uri: " + uri);
        return null;
    }

    private boolean isInBatchMode() {
        return mIsInBatchMode.get() != null && mIsInBatchMode.get();
    }
    //</editor-fold>


    //<editor-fold desc="other functions">

    /**
     * I do not really use analytics, but if you export
     * your content provider it makes sense to do so, to get
     * a feeling for client usage. Especially if you want to
     * _change_ something which might break existing clients,
     * please check first if you can safely do so.
     */
    private void doAnalytics(Uri uri, String event) {
       /* if (BuildConfig.DEBUG) {
            Log.v("dzumi", event + " -> " + uri);
            Log.v("dzumi", "caller: " + detectCaller());
        }*/
    }

    /**
     * You can use this for Analytics.
     * <p/>
     * Be aware though: This might be costly if many apps
     * are running.
     */
    private String detectCaller() {
        // found here:
        // https://groups.google.com/forum/#!topic/android-developers/0HsvyTYZldA
        int pid = Binder.getCallingPid();
        return getProcessNameFromPid(pid);
    }

    /**
     * Returns the name of the process the pid belongs to. Can be null if neither
     * an Activity nor a Service could be found.
     *
     * @param givenPid
     * @return
     */
    private String getProcessNameFromPid(int givenPid) {
        ActivityManager am = (ActivityManager) getContext().getSystemService(
                Activity.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> lstAppInfo = am
                .getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo ai : lstAppInfo) {
            if (ai.pid == givenPid) {
                return ai.processName;
            }
        }
        // added to take care of calling services as well:
        List<ActivityManager.RunningServiceInfo> srvInfo = am
                .getRunningServices(Integer.MAX_VALUE);
        for (ActivityManager.RunningServiceInfo si : srvInfo) {
            if (si.pid == givenPid) {
                return si.process;
            }
        }
        return null;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void logQuery(SQLiteQueryBuilder builder, String[] projection, String selection, String sortOrder) {
       /* if (BuildConfig.DEBUG) {
            Log.v("dzumi", "query: " + builder.buildQuery(projection, selection, null, null, sortOrder, null));
        }*/
    }

    @SuppressWarnings("deprecation")
    private void logQueryDeprecated(SQLiteQueryBuilder builder, String[] projection, String selection, String sortOrder) {
       /* if (BuildConfig.DEBUG) {
            Log.v("dzumi", "query: " + builder.buildQuery(projection, selection, null, null, null, sortOrder, null));
        }*/
    }

    @Override
    public void clearAllData() {
        delete(DBContract.Country.CONTENT_URI, null, null);
    }

    @Override
    public Cursor rawQuery(String sql, String[] args) {
        SQLiteDatabase db = mDbContext.getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, args);

        //?
        cursor.setNotificationUri(getContext().getContentResolver(), DBContract.CONTENT_URI);

        return cursor;
    }

    //</editor-fold>

}