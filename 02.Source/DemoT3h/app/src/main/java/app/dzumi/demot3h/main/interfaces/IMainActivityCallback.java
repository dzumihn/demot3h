package app.dzumi.demot3h.main.interfaces;

import java.util.List;
import java.util.Map;

/**
 * Created by dzumi on 08/03/2015.
 */
public interface IMainActivityCallback {
    List<Map<String, Object>> initData();
}
