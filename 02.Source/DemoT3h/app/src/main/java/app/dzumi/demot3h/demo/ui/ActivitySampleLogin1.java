package app.dzumi.demot3h.demo.ui;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import app.dzumi.demot3h.R;

public class ActivitySampleLogin1 extends ActionBarActivity implements View.OnClickListener{

    EditText edtEmail, edtPass;
    Button btnContinue;

    View.OnClickListener mOnClickListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_sample_login);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtPass = (EditText) findViewById(R.id.edtPass);
        btnContinue = (Button) findViewById(R.id.btnContinue);

        mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = v.getId();
                if(id == R.id.btnContinue){

                }/*else if()*/
            }
        };

        //cach 1:
       /* btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = edtEmail.getText().toString();
                String pass = edtPass.getText().toString();
                if(email.equals("midu") && pass.equals("midu"))
                {
                    Toast.makeText(ActivitySampleLogin1.this,"OK", Toast.LENGTH_SHORT).show();

                }
                else
                    //xuat thong bao
                    Toast.makeText(ActivitySampleLogin1.this,"Sai", Toast.LENGTH_SHORT).show();
            }
        });*/
        //cach 2:
//        btnContinue.setOnClickListener(mOnClickListener);

        //cach 3:
        btnContinue.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_sample_login1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if(id == R.id.btnContinue){
            String email = edtEmail.getText().toString();
            String pass = edtPass.getText().toString();
            if(email.equals("midu") && pass.equals("midu"))
            {
//                Toast.makeText(this,"OK", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this,ActivityDemoUI.class);
                startActivity(intent);
            }
            else
                //xuat thong bao
                Toast.makeText(this,"Sai", Toast.LENGTH_SHORT).show();
        }
    }
}
