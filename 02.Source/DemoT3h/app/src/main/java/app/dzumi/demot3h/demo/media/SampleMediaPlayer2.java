package app.dzumi.demot3h.demo.media;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import app.dzumi.demot3h.R;

public class SampleMediaPlayer2 extends ActionBarActivity implements
        View.OnClickListener, SeekBar.OnSeekBarChangeListener, MediaPlayer.OnPreparedListener {

    TextView tvTimer, tvTotalTime, tvName;
    SeekBar skbVolumn, skbTimer;
    Button btnStart, btnPause, btnStop;

    MediaPlayer mediaPlayer;//play audio
    AudioManager audioManager;//quan ly am luong
    Handler handler;

    int totalTime;
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            //thiet lap thoi gian hien tai
//            tvTimer.setText();
            skbTimer.setProgress(mediaPlayer.getCurrentPosition());

            if(mediaPlayer.isPlaying())
            {
                handler.postDelayed(runnable, 1000);
            }
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_media_player2);

        tvName = (TextView) findViewById(R.id.tvName);
        tvTimer = (TextView) findViewById(R.id.tvTimer);
        tvTotalTime = (TextView) findViewById(R.id.tvTotalTime);

        btnPause = (Button) findViewById(R.id.btnPause);
        btnStop = (Button) findViewById(R.id.btnStop);
        btnStart = (Button) findViewById(R.id.btnPlay);

        btnPause.setOnClickListener(this);
        btnStart.setOnClickListener(this);
        btnStop.setOnClickListener(this);

        skbTimer = (SeekBar) findViewById(R.id.skbTimer);
        skbVolumn = (SeekBar) findViewById(R.id.skbVolumn);

        skbTimer.setOnSeekBarChangeListener(this);
        skbVolumn.setOnSeekBarChangeListener(this);
        //initdata
        mediaPlayer = MediaPlayer.create(this, R.raw.song);
        mediaPlayer.setOnPreparedListener(this);
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        handler = new Handler();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sample_media_player2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if(id == R.id.btnPause){
            if(mediaPlayer.isPlaying())
                mediaPlayer.pause();
        }else if(id == R.id.btnPlay)
        {
            mediaPlayer.start();
            handler.postDelayed(runnable, 1000);

            //
        }else if(id == R.id.btnStop)
        {
            mediaPlayer.stop();//khong play lai dc
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        //seekbar
        int id = seekBar.getId();
        if(id == R.id.skbVolumn)
        {
            //thiet lap lai gia tri am luong
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,progress,AudioManager.FLAG_PLAY_SOUND|AudioManager.FLAG_VIBRATE);
        }else if(id == R.id.skbTimer)
        {

        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        if(seekBar.getId() == R.id.skbTimer)
        {
            mediaPlayer.seekTo(seekBar.getProgress());
        }
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        totalTime = mp.getDuration();

        //set thoi gian duoi dinh dang: %1$d m, %2$d s
//        tvTotalTime.setText(totalTime);

        skbTimer.setMax(totalTime);
        skbTimer.setProgress(mp.getCurrentPosition()); //thiet lap gia tri hien tai cua bai hat
        //thiet lap gia tri am luong toi da
        skbVolumn.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        skbVolumn.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
    }
}
