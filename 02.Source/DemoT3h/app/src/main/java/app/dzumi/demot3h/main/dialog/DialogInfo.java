package app.dzumi.demot3h.main.dialog;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import app.dzumi.demot3h.R;

public class DialogInfo extends BaseDialog {
	Bundle argument;
	String[] notice;
	IActionDialogInfo listener;
	public static DialogInfo newInstance(IActionDialogInfo listener, Bundle bundle, String... notice ) {
		DialogInfo frag = new DialogInfo();
		frag.argument = bundle;
		frag.notice = notice;
		frag.listener = listener;
		return frag;
	}


	@Override
	@NonNull
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		//create
		String title = argument.getString( DIALOG_TITLE);
		String content = argument.getString(DIALOG_CONTENT);
		if(content == null)
			content = "";
		int type = argument.getInt(DIALOG_TYPE);
		
		//
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).
				setTitle(title);

		//
		switch (type) {
		case DIALOG_TYPE_OK: //one choice button
			builder.setPositiveButton(notice[0],new OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					if(argument.getBoolean(DIALOG_OK_HAVE_EVENT))
					{
						listener.onPositiveButtonClick();
					}
					dismiss();
				}});
			break;
		case DIALOG_TYPE_YES_NO:
			builder.setPositiveButton(notice[0],new OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {

					listener.onPositiveButtonClick();
					dismiss();
				}})
				.setNegativeButton(notice[1], new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						listener.onNegativeButtonClick();
						dismiss();
					}
				});

			break;
		default:
			break;
		}
		
		AlertDialog alertDialog = builder.create();
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View view = inflater.inflate(R.layout.dialog_info, null);
		TextView contentView = (TextView) view.findViewById(R.id.tvInfo); 
		//set text

		contentView.setText(Html.fromHtml(content), TextView.BufferType.SPANNABLE);
		alertDialog.setView(view);
		return alertDialog;
	}

    /**
     * There is some risk in using commitAllowingStateLoss - the documentation states "Like commit() but allows the commit to be executed after an activity's state is saved.
     * This is dangerous because the commit can be lost if the activity needs to later be restored from its state,
     * so this should only be used for cases where it is okay for the UI state to change unexpectedly on the user.
     * @param manager
     * @param tag
     */
    @Override
    public void show(FragmentManager manager, String tag) {
        Log.i("dzumi", "show...");
        FragmentTransaction ft = manager.beginTransaction();
        ft.add(this, tag);
        ft.commitAllowingStateLoss();
    }
}
