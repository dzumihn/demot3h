package app.dzumi.demot3h.main.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import app.dzumi.demot3h.R;
import app.dzumi.demot3h.main.utils.Log;

public class ImageFragment extends BaseFragment {
    public final static String TAG = "ImageFragment";
    public final static String EXTRA_PATH = "path";
    private ImageView imageView;

    @Override
    protected int layoutID() {
        return R.layout.fragment_image_view;
    }

    @Override
    protected void initViews(View root, Bundle savedInstanceState) {
        imageView = findView(R.id.imageView);

    }

    @Override
    protected void setupViews() {
        String file = ((Uri)getArguments().getParcelable(EXTRA_PATH)).getPath();
        Log.d(file);
        Bitmap bitmap = BitmapFactory.decodeFile(file);
        imageView.setImageBitmap(bitmap);
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }

}