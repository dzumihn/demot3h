package app.dzumi.demot3h.demo.internet.xml;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import app.dzumi.demot3h.main.utils.Log;

/**
 * Created by dzumi on 20/07/2015.
 */
public class RSSUtils {
    public static List<News> readRSSTinhTe(InputStream inputStream)
    {
        List<News> listNews = new ArrayList<>();

        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder documentBuilder = builderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(inputStream);
            if(document!= null) {
                NodeList items = document.getElementsByTagName(News.ITEMS);
                if(items != null)
                {
                    News news = null;
                    for(int i = 0; i < items.getLength(); i++)
                    {
                        news = new News();
                        NodeList childrent = items.item(i).getChildNodes();
                        if(childrent != null)
                        {
                            for(int j = 0; j < childrent.getLength(); j++)
                            {
                                Node child = childrent.item(j);
                                switch (child.getNodeName())
                                {
                                    case News.TITLE:
                                        news.setTitle(child.getTextContent());
                                        break;
                                    case News.DESCRIPTION:
                                        //cat
                                        String text = child.getTextContent();
                                        Log.d("RSS-DEMO",text);
                                        int startImage = text.indexOf("<img");
                                        int endImage = text.indexOf("/>");

                                        Log.d("RSS-DEMO",startImage + "/" + endImage);

                                        String subString = text.substring(startImage, endImage+1);

                                        Log.d("RSS-DEMO","sub:" + subString);

                                        text = text.replace(subString, "");

                                        news.setLinkImage(subString.split("<img src=\"")[1].split("\"")[0]);
                                        news.setDescription(text);

                                        /*String[] temp = text.split("</div><br />");
                                        //temp[0]: hinh
                                        news.setLinkImage(temp[0].split("<img src=\"")[1].split("\"")[0]);
                                        //temp[1]: description
                                        news.setDescription(temp[1].substring(0, temp[1].length()-3));*/
                                        break;
                                    case News.LINK:
                                        news.setLink(child.getTextContent());
                                        break;
                                    case News.PUB_DATE:
                                        news.setPubDate(child.getTextContent());
                                        break;
                                }
                            }
                        }
                        listNews.add(news);
                    }

                }
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return listNews;
    }

    public static List<News> readRSS24h(InputStream inputStream){
        List<News> newsList = new ArrayList<>();

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = null;
        try {
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(inputStream);
            if(document != null)
            {
                NodeList nodeListItem = document.getElementsByTagName(News.ITEMS);
                if(nodeListItem != null){
                    News news = null;
                    for(int i = 0; i < nodeListItem.getLength(); i++){
                        news = new News();
                        Node item = nodeListItem.item(i);
                        NodeList nodeListChildrent = item.getChildNodes();
                        if(nodeListChildrent!= null){
                            for(int j = 0; j < nodeListChildrent.getLength(); j++){
                                Node child = nodeListChildrent.item(j);
                                String text = child.getTextContent();
                                switch (child.getNodeName()){
                                    case News.TITLE:
                                        news.setTitle(text);
                                        break;
                                    case News.DESCRIPTION:
                                        news.setDescription(text);
                                        break;
                                    case News.LINK:
                                        news.setLink(text);
                                        break;
                                    case News.LINK_IMAGE:
                                        news.setLinkImage(text);
                                        break;
                                    case News.PUB_DATE:
                                        news.setPubDate(text);
                                        break;
                                }
                            }
                        }

                        newsList.add(news);
                    }
                }
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return newsList;
    }
}
