package app.dzumi.demot3h.demo.resources;

import android.app.Activity;
import android.graphics.drawable.LevelListDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.util.Timer;
import java.util.TimerTask;

import app.dzumi.demot3h.R;
import app.dzumi.demot3h.main.utils.Log;

public class DrawableDemoActivity extends Activity implements View.OnClickListener {
    ImageButton btnTransition;
    ImageView ivLevelList;
    private TransitionDrawable drawable;
    private LevelListDrawable mLevelListDrawable;

    void initView() {
        btnTransition = (ImageButton) findViewById(R.id.btnTransition);
        ivLevelList = (ImageView) findViewById(R.id.btnLevelList);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_drawable_demo);
        initView();
        /* sai
        TransitionDrawable drawable = (TransitionDrawable) getResources()
                .getDrawable(R.drawable.transiton_drawable);*/
        /* sai vi trong layout thiet lap thuoc tinh background
        TransitionDrawable drawable = (TransitionDrawable) btnTransition.
                getDrawable();*/
        drawable = (TransitionDrawable) btnTransition.
                getBackground();

        btnTransition.setOnClickListener(this);
        drawable.startTransition(500);

        demoLevelList();
    }

    int mLevel = 5;
    boolean isRunning = false;

    //demo levellist
    void demoLevelList() {
        mLevelListDrawable = (LevelListDrawable) ivLevelList.getBackground();
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            Log.d("level: " + mLevel);
            mLevelListDrawable.setLevel(mLevel);
            if (mLevel == 0)
                mLevel = 5;
            mLevel--;
            if(isRunning)
                ivLevelList.postDelayed(runnable, 500);

        }
    };
    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btnTransition) {
            //demo transition drawable
            drawable.reverseTransition(500);

            //demo level list


            //cach 1: chay tay
//            Log.d("level: " + mLevel);
//            ivLevelList.setImageLevel(mLevel); //khong chay duoc
            /*mLevelListDrawable.setLevel(mLevel);
            if(mLevel == 0)
                mLevel = 5;
            mLevel --;*/

            //cach 2: chay tu dong
            if (isRunning) //isRunning == true
            {
                //stop //BTVN
            } else //isRunning == false
            {
                //start
                //dung handler de xu ly
                //Only the original thread that created a view hierarchy can touch its views.
               /* Timer timer = new Timer();
                timer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        Log.d("level: " + mLevel);
                        mLevelListDrawable.setLevel(mLevel);
                        if (mLevel == 0)
                            mLevel = 5;
                        mLevel--;
                    }
                }, 1000, 500);*/

                ivLevelList.postDelayed(runnable, 500);
            }
            isRunning = !isRunning;//cap nhat lai gia tri cho isRunning
        }
    }
}
