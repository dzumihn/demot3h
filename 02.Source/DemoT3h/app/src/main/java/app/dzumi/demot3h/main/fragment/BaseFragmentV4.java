package app.dzumi.demot3h.main.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by dzumi on 07/03/2015.
 */
public abstract class BaseFragmentV4 extends Fragment {
    public final static String ITEMS = "items";
    private View root;
    protected Context mContext;

    protected abstract int layoutID();
    protected abstract void initViews(View root, Bundle savedInstanceState);
    protected abstract void setupViews();

    public  <V extends View> V findView( int viewId) {
        return (V) root.findViewById(viewId);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContext = getActivity();
        root = inflater.inflate(layoutID(), container, false);
        initViews(root, savedInstanceState);
        setupViews();
        return root;
    }
}
