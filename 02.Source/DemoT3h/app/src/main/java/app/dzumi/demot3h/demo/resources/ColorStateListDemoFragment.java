package app.dzumi.demot3h.demo.resources;

import android.os.Bundle;
import android.view.View;

import app.dzumi.demot3h.R;
import app.dzumi.demot3h.main.fragment.BaseFragment;

/**
 * Created by dzumi on 01/05/2015.
 */
public class ColorStateListDemoFragment extends BaseFragment {
    static final String TAG = "ColorStateListDemoFragment";
    @Override
    protected int layoutID() {
        return R.layout.fragment_color_state_demo;
    }

    @Override
    protected void initViews(View root, Bundle savedInstanceState) {
    }

    @Override
    protected void setupViews() {
    }

    @Override
    public String getTagFragment() {
        return TAG;
    }
}
