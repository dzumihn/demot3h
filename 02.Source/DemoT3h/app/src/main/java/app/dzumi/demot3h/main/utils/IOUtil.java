package app.dzumi.demot3h.main.utils;

import android.content.Context;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by dzumi on 04/06/2015.
 */
public class IOUtil {
    Context mContext;
    public IOUtil(Context context)
    {
        mContext = context;
    }

    public void deleteExternalStoragePrivateFolder(String path) {
        // Get path for the file on external storage.  If external
        // storage is not currently mounted this will fail.
        File file = new File(mContext.getExternalFilesDir(null), path);
        Log.d("folder: " + file.getAbsolutePath());
        deleteDirectory(file);
    }

    public boolean deleteDirectory(File directory) {
        if(directory.exists()){
            File[] files = directory.listFiles();
            if(null!=files){
                for(int i=0; i<files.length; i++) {
                    if(files[i].isDirectory()) {
                        deleteDirectory(files[i]);
                    }
                    else {
                        files[i].delete();
                    }
                }
            }
        }
        return(directory.delete());
    }


    public static boolean deleteFolder(String path) {
        File file = new File(path);
        if (file.exists())
            return file.delete();
        return false;
    }

    public static boolean delete(String path) {
        File file = new File(path);
        if (file.exists())
            return file.delete();
        return false;
    }

    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i=0; i<children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        // The directory is now empty so delete it
        return dir.delete();
    }


    public String readTextFile(String pathFile)
    {
        StringBuilder result = new StringBuilder();
        try {
            FileReader fileReader = new FileReader(pathFile);
            BufferedReader br = new BufferedReader(fileReader);

            int i;
            char[] buffer = new char[512];
            while((i = br.read(buffer))!= -1)
            {
                result.append(new String(buffer, 0, i));
            }
            br.close();
            fileReader.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        }
        return result.toString();
    }
    public boolean appendStringFileCount(String pathFile, String text)
    {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(pathFile, true);
            BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);
            text = text+ "\n";
            bos.write(text.getBytes());
            bos.flush();
            bos.close();

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
        return true;

    }

    public boolean internalFileExistance(String fname){
        File file = mContext.getFileStreamPath(fname);
        return file.exists();
    }

    public void copyFileToInternal(InputStream inputStream, String name)
    {
        try {
            FileOutputStream fileOutputStream = mContext.openFileOutput(name, Context.MODE_PRIVATE);
            byte[] buffer = new byte[512];
            int i = 0;
            while ((i = inputStream.read(buffer)) != -1)
            {
                fileOutputStream.write(buffer, 0, i);
            }
            fileOutputStream.close();
            inputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
