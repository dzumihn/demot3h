package app.dzumi.demot3h.demo.sqlite.contentprovider.base;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by dzumi on 11/06/2015.
 */
public class DBContract {
    /**
     * The authority of the mPDS provider.
     */
    public static final String AUTHORITY =
            "app.dzumi.provider.demot3h";
    /**
     * The content URI for the top-level
     * mpds authority.
     * Using for raw query
     */


    public static final Uri CONTENT_URI =
            Uri.parse("content://" + AUTHORITY);

    public static final Uri CONTENT_URI_RAW_QUERY =
            Uri.withAppendedPath(CONTENT_URI, "rawQuery");

    public static interface CommonColumns
            extends BaseColumns {
    }

    public static class Country implements CommonColumns{

        public static final String NAME_EN = "nameEn";
        public static final String NAME_VI = "nameVi";
        public static final String FLAG = "flag";
        public static final String POPULATION = "population";
        public static final String CAPTION = "caption";

        public static final String TABLE_NAME = "Country";
        public static final String TABLE_NAME_ID = "Country/#";
        public static final String CONTENT_TYPE_VAlUE = "/app.dzumi.demot3h_country";

        /**
         * The content URI for this table.
         */
        public static final Uri CONTENT_URI =
                Uri.withAppendedPath(DBContract.CONTENT_URI, TABLE_NAME);
        /**
         * The mime type of a directory of items.
         */
        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + CONTENT_TYPE_VAlUE;
        /**
         * The mime type of a single item.
         */
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + CONTENT_TYPE_VAlUE;
        /**
         * A projection of all columns
         * in the items table.
         */
        public static final String[] PROJECTION_ALL =
                {_ID, NAME_EN, NAME_VI, FLAG, POPULATION, CAPTION};

        /**
         * The default sort order for
         * queries containing NAME fields.
         */
        public static final String SORT_ORDER_DEFAULT = NAME_EN + " ASC";
    }
}
