package app.dzumi.demot3h.demo.asyntask;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import app.dzumi.demot3h.BaseActivity;
import app.dzumi.demot3h.R;
import app.dzumi.demot3h.demo.adapter.model.Country;
import app.dzumi.demot3h.demo.adapter.adapter.CountryAdapter2;

/**
 * Created by dzumi on 29/06/2015.
 */
public class ActivitySampleAsynTask extends BaseActivity implements View.OnClickListener {
    Button btnNormal, btnAsyntask;
    GridView gridView;
    ProgressBar progressBar;
    CountryAdapter2 mCountryAdapter;
    List<Country> mCountries;
    @Override
    protected Fragment initFragment() {
        return null;
    }

    @Override
    protected View initContentView() {
        return getView(R.layout.activity_thread_sample_asyntask);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gridView = (GridView) findViewById(R.id.gridView);
        btnAsyntask = (Button) findViewById(R.id.btnAsyntask);
        btnNormal = (Button) findViewById(R.id.btnNormal);
        btnAsyntask.setOnClickListener(this);
        btnNormal.setOnClickListener(this);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if(id == R.id.btnAsyntask)
        {
            progressBar.setVisibility(View.VISIBLE);
            mCountries = new ArrayList<>();
            mCountryAdapter = new CountryAdapter2(this, R.layout.item_list_view_countries1, mCountries);
            gridView.setAdapter(mCountryAdapter);

            myAsyncTask.execute();
        }else if(id == R.id.btnNormal)
        {
            progressBar.setVisibility(View.VISIBLE);
            mCountries = new ArrayList<>();
            for(int i = 0; i < 10; i++)
            {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                mCountries.add(new Country("vietnam", "Vi?t Nam", R.drawable.vietnam));
            }
            mCountryAdapter = new CountryAdapter2(this, R.layout.item_list_view_countries1, mCountries);
            gridView.setAdapter(mCountryAdapter);
            progressBar.setVisibility(View.GONE);

        }
    }

    AsyncTask<Void, Void, Void> myAsyncTask = new AsyncTask<Void, Void, Void>() {
        @Override
        protected Void doInBackground(Void... params) {
            for(int i = 0; i < 10; i++)
            {
                try {
                    //gia lap thoi gian download hinh tu server
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                mCountries.add(new Country("vietnam", "Vi?t Nam", R.drawable.vietnam));
                publishProgress(null);
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            mCountryAdapter.notifyDataSetChanged();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressBar.setVisibility(View.GONE);
        }
    };


}
