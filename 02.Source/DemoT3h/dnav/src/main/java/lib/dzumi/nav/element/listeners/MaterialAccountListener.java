package lib.dzumi.nav.element.listeners;

import lib.dzumi.nav.element.MaterialAccount;

public interface MaterialAccountListener {

    public void onAccountOpening(MaterialAccount account);

    public void onChangeAccount(MaterialAccount newAccount);

}