package lib.dzumi.nav;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import lib.dzumi.nav.element.MaterialAccount;
import lib.dzumi.nav.element.MaterialSection;
import lib.dzumi.nav.element.listeners.MaterialSectionListener;
import lib.dzumi.nav.util.MaterialDrawerLayout;

import static lib.dzumi.nav.element.MaterialAccount.OnAccountDataLoaded;

/**
 * Fragment used for managing interactions for and presentation of a navigation drawerLayout.
 * See the <a href="https://developer.android.com/design/patterns/navigation-drawerLayout.html#Interaction">
 * design guidelines</a> for a complete explanation of the behaviors implemented here.
 */
public abstract class NavigationDrawerFragment extends Fragment implements MaterialSectionListener, OnAccountDataLoaded {

    //region Constants variable
    private static final int ELEMENT_TYPE_SECTION = 0;
    private static final int ELEMENT_TYPE_DIVISOR = 1;
    private static final int ELEMENT_TYPE_SUBHEADER = 2;
    private static final int ELEMENT_TYPE_BOTTOM_SECTION = 3;


    public static final int BOTTOM_SECTION_START = 10000;
    private static final int USER_CHANGE_TRANSITION = 500;

    private static final int DRAWERHEADER_ACCOUNTS = 0;

    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";
    private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";
    //endregion

    private View mFragmentContainerView;

    private int mCurrentSelectedPosition = 0;
    private boolean mFromSavedInstanceState;
    private boolean mUserLearnedDrawer;

    MaterialDrawerLayout materialDrawerLayout;
    private RelativeLayout drawerLayout;
    private TextView tvUserName;
    private TextView tvDescription;
    private ImageView ivAvatar;
    private ImageView ivCover;

    private int drawerColor; //mau nen drawerLayout
    private LinearLayout layout_sections;
    private LinearLayout layout_bottomSections;

    public LinkedList<MaterialSection> getSectionList() {
        return sectionList;
    }

    private LinkedList<MaterialSection> sectionList;
    private LinkedList<MaterialSection> bottomSectionList;

    private MaterialSection currentSection;
    private MaterialAccount materialAccount;

    private List<Integer> elementsList;
    private Resources resources;
    private float density;
    private int drawerHeaderType;
    private boolean rippleSupport;
    private boolean kitkatTraslucentStatusbar = false;
    Context context;
    private Resources.Theme theme;
    private TypedValue typedValue;


    //region abstract class & interface callback & constructor
    public abstract void init(Bundle savedInstanceState);

    public NavigationDrawerFragment() {
    }
    //endregion


    //region main life cycle
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Indicate that this fragment would like to influence the set of actions in the action bar.
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_drawer, container, false);

        initView(view);

        sectionList = new LinkedList<>();
        bottomSectionList = new LinkedList<>();
        elementsList = new ArrayList<>();

        // set drawerLayout backgrond color
        drawerLayout.setBackgroundColor(drawerColor);

        //get resources and density
        resources = this.getResources();
        density = resources.getDisplayMetrics().density;

        // if device is kitkat, chua xac dinh cach dung
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
            TypedArray windowTraslucentAttribute = theme.obtainStyledAttributes(new int[]{android.R.attr.windowTranslucentStatus});
            kitkatTraslucentStatusbar = windowTraslucentAttribute.getBoolean(0, false);
            if (kitkatTraslucentStatusbar) {
                Window window = getActivity().getWindow();
                window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                        WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

                if (drawerHeaderType == DRAWERHEADER_ACCOUNTS) {
                    RelativeLayout.LayoutParams photoParams = (RelativeLayout.LayoutParams) ivAvatar.getLayoutParams();
                    photoParams.setMargins((int) (16 * density), resources.getDimensionPixelSize(R.dimen.traslucentPhotoMarginTop), 0, 0);
                    ivAvatar.setLayoutParams(photoParams);
                }
            }
        }

        // DEVELOPER CALL TO INIT
        init(savedInstanceState);

        if (sectionList.size() == 0) {
            throw new RuntimeException("You must add at least one Section to top list.");
        }

        //init section
        MaterialSection section = null;
        if (savedInstanceState == null) {

            // init account views
            if (materialAccount != null)
                notifyAccountDataChanged();

            // init section
            section = sectionList.get(0);

        }/* else {

            ArrayList<Integer> accountNumbers = savedInstanceState.getIntegerArrayList(STATE_ACCOUNT);

            notifyAccountDataChanged();

            int accountSelected = savedInstanceState.getInt(STATE_SECTION);

            if (accountSelected >= BOTTOM_SECTION_START) {
                section = bottomSectionList.get(accountSelected - BOTTOM_SECTION_START);
            } else
                section = sectionList.get(accountSelected);

        }*/
        section.select();

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Forward the new configuration the drawerLayout toggle component.
//        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    //endregion


    //region init
    private void initView(View view) {
//        materialDrawerLayout = (MaterialDrawerLayout) view.findViewById(R.id.drawer);

        drawerLayout = (RelativeLayout) view.findViewById(R.id.drawer);

        layout_sections = (LinearLayout) view.findViewById(R.id.layout_sections);
        layout_bottomSections = (LinearLayout) view.findViewById(R.id.layout_bottom_sections);



        tvUserName = (TextView) view.findViewById(R.id.tvUserName);
        tvDescription = (TextView) view.findViewById(R.id.tvDescription);
        ivAvatar = (ImageView) view.findViewById(R.id.ivAvatar);
        ivCover = (ImageView) view.findViewById(R.id.ivCover);


        initTheme();

        // get and set username and mail text colors
       /* theme.resolveAttribute(R.attr.accountStyle, typedValue, true);
        TypedArray attributes = theme.obtainStyledAttributes(typedValue.resourceId, R.styleable.MaterialAccount);
        try {
            tvUserName.setTextColor(attributes.getColor(R.styleable.MaterialAccount_titleColor, 0xFFF));
            tvDescription.setTextColor(attributes.getColor(R.styleable.MaterialAccount_subtitleColor, 0xFFF));
        } finally {
            attributes.recycle();
        }*/


    }

    private void initTheme() {
        theme = getActivity().getTheme();
        typedValue = new TypedValue();
        theme.resolveAttribute(R.attr.rippleBackport, typedValue, false);
        rippleSupport = typedValue.data != 0;
     /*   theme.resolveAttribute(R.attr.uniqueToolbarColor, typedValue, false);
        uniqueToolbarColor = typedValue.data != 0;
        theme.resolveAttribute(R.attr.multipaneSupport, typedValue, false);
        multiPaneSupport = typedValue.data != 0;*/
        theme.resolveAttribute(R.attr.drawerColor, typedValue, true);
        drawerColor = typedValue.data;
    }
    //endregion

    public boolean isDrawerOpen() {
        return materialDrawerLayout != null && materialDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

    /**
     * Users of this fragment must call this method to set up the navigation drawerLayout interactions.
     *
     * @param fragmentId   The android:id of this fragment in its activity's layout.
     * @param drawerLayout The DrawerLayout containing this fragment's UI.
     */
    public void setUp(int fragmentId, DrawerLayout drawerLayout) {
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        materialDrawerLayout = (MaterialDrawerLayout) drawerLayout;

        // set a custom shadow that overlays the main content when the drawerLayout opens
        materialDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawerLayout's list view with items and click listener


    }


    //region option menu
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
    //endregion

    @Override
    public void onClick(MaterialSection section) {
        currentSection = section;

        int position = section.getPosition();

        for (MaterialSection mySection : sectionList) {
            if (position != mySection.getPosition())
                mySection.unSelect();
        }
        for (MaterialSection mySection : bottomSectionList) {
            if (position != mySection.getPosition())
                mySection.unSelect();
        }
        materialDrawerLayout.closeDrawers();

    }





    //region add item nav
    public void addSection(MaterialSection section) {
        section.setPosition(sectionList.size());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) (48 * density));
        sectionList.add(section);
        layout_sections.addView(section.getView(), params);

        // add the element to the list
        elementsList.add(ELEMENT_TYPE_SECTION);
    }

    public void addBottomSection(MaterialSection section) {
        section.setPosition(BOTTOM_SECTION_START + bottomSectionList.size());
//        section.setTypeface(fontManager.getRobotoRegular());
        bottomSectionList.add(section);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) (48 * density));
        layout_bottomSections.addView(section.getView(), params);
        // add the element to the list
        elementsList.add(ELEMENT_TYPE_BOTTOM_SECTION);
    }


    public void addAccount(MaterialAccount account) {
        if (DRAWERHEADER_ACCOUNTS != drawerHeaderType) {
            throw new RuntimeException("Your header is not setted to Accounts, check in your styles.xml");
        }

        materialAccount = account;
        materialAccount.setAccountListener(this);
//        account.setAccountListener(this);

    }
    //endregion

    //region create section item
    public MaterialSection newSection(String title, Drawable icon, MaterialSectionListener target, String tag) {
        MaterialSection section = newSection(title, icon, target);
        section.setTag(tag);
        return section;
    }
    public MaterialSection newSection(String title, Bitmap icon, MaterialSectionListener target, String tag) {
        MaterialSection section = newSection(title, icon, target);
        section.setTag(tag);
        return section;
    }
    public MaterialSection newSection(String title, int icon, MaterialSectionListener target, String tag) {
        MaterialSection section = newSection(title, icon, target);
        section.setTag(tag);
        return section;
    }

    public MaterialSection newSection(String title, Drawable icon, MaterialSectionListener target) {
        MaterialSection section = new MaterialSection<Fragment>(context, MaterialSection.ICON_24DP, rippleSupport, MaterialSection.TARGET_LISTENER);
        section.setOnClickListener(this);
        section.setIcon(icon);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }


    public MaterialSection newSection(String title, Bitmap icon, MaterialSectionListener target) {
        MaterialSection section = new MaterialSection<Fragment>(context, MaterialSection.ICON_24DP, rippleSupport, MaterialSection.TARGET_LISTENER);
        section.setOnClickListener(this);
        section.setIcon(icon);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }


    public MaterialSection newSection(String title, int icon, MaterialSectionListener target) {
        return newSection(title, resources.getDrawable(icon), target);
    }

    @SuppressWarnings("unchecked")
    public MaterialSection newSection(String title, MaterialSectionListener target) {
        MaterialSection section = new MaterialSection<Fragment>(context, MaterialSection.ICON_NO_ICON, rippleSupport, MaterialSection.TARGET_LISTENER);
        section.setOnClickListener(this);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }
    //endregion

    //region set info account & override function for load image
    public void setAccountAvatar(Drawable photo) {
        if (DRAWERHEADER_ACCOUNTS != drawerHeaderType) {
            throw new RuntimeException("Your header is not setted to Accounts, check in your styles.xml");
        }

        ivAvatar.setImageDrawable(photo);
    }

    public void setDrawerHeaderImage(int backgroundId) {
        setDrawerHeaderImage(resources.getDrawable(backgroundId));
    }

    public void setDrawerHeaderImage(Drawable background) {
        switch (drawerHeaderType) {

            case DRAWERHEADER_ACCOUNTS:
                ivCover.setImageDrawable(background);
                break;
            default:
                throw new RuntimeException("Your drawerLayout layout configuration don't support a background image, check in your styles.xml");
        }
    }

    public void setDescription(String description) {
        if (DRAWERHEADER_ACCOUNTS != drawerHeaderType) {
            throw new RuntimeException("Your header is not setted to Accounts, check in your styles.xml");
        }

        tvDescription.setText(description);
    }

    public void setDescriptionTextColor(int color) {
        if (DRAWERHEADER_ACCOUNTS != drawerHeaderType) {
            throw new RuntimeException("Your header is not setted to Accounts, check in your styles.xml");
        }

        tvDescription.setTextColor(color);
    }

    public void setUsername(String username) {
        if (DRAWERHEADER_ACCOUNTS != drawerHeaderType) {
            throw new RuntimeException("Your header is not setted to Accounts, check in your styles.xml");
        }

        this.tvUserName.setText(username);
    }

    public void setUsernameTextColor(int color) {
        if (DRAWERHEADER_ACCOUNTS != drawerHeaderType) {
            throw new RuntimeException("Your header is not setted to Accounts, check in your styles.xml");
        }

        this.tvUserName.setTextColor(color);
    }

    /**
     * Reload Application data from Account Information
     */
    public void notifyAccountDataChanged() {
        this.setAccountAvatar(materialAccount.getCircularPhoto());
        this.setDrawerHeaderImage(materialAccount.getBackground());
        this.setUsername(materialAccount.getTitle());
        this.setDescription(materialAccount.getDescription());
    }

    @Override
    public void onUserPhotoLoaded(MaterialAccount account) {
        notifyAccountDataChanged();
    }

    @Override
    public void onBackgroundLoaded(MaterialAccount account) {
        notifyAccountDataChanged();
    }


    //endregion


}
