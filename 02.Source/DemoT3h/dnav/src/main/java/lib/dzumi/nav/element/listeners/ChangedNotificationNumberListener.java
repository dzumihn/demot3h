package lib.dzumi.nav.element.listeners;

/**
 * Created by dzumi on 02/03/2015.
 */
public interface ChangedNotificationNumberListener {
    public void onChanged(int position, int number);

}
