package lib.dzumi.nav.element.listeners;

import lib.dzumi.nav.element.MaterialSection;

public interface MaterialSectionListener {

    public void onClick(MaterialSection section);
}